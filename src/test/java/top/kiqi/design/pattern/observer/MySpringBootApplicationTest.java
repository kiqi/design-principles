package top.kiqi.design.pattern.observer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.junit4.SpringRunner;
import top.kiqi.design.pattern.behavioral.observer.MySpringBootApplication;
import top.kiqi.design.pattern.behavioral.observer.spring.MyEvent;

// Spring提供的事件监听(发布订阅)模式 - 采用SpringBootTest进行测试
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MySpringBootApplication.class)
public class MySpringBootApplicationTest implements ApplicationContextAware {
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void test(){
        // 定义事件，并将事件发布到applicationContext中。
        // 具体的监听者在springBoot启动时，所有实现ApplicationListener<MyEvent>接口的spring bean对象均会注册到ApplicationEventMulticaster中，根据事件类型被通知调用
        MyEvent order = new MyEvent(this, "kiqi的消息");
        applicationContext.publishEvent(order);
     }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
