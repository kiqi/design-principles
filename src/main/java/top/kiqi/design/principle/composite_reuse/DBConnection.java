package top.kiqi.design.principle.composite_reuse;

public abstract class DBConnection {
    public abstract String getConnection();
}
