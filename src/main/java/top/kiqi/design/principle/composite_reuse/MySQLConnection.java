package top.kiqi.design.principle.composite_reuse;

public class MySQLConnection extends DBConnection{
    @Override
    public String getConnection() {
        return "MySQL connection!";
    }
}
