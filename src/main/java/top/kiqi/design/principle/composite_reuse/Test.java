package top.kiqi.design.principle.composite_reuse;

public class Test {
    // 合成复用原则 - 为避免继承泛滥，应尽量先使用组合或者聚合等关联关系(黑箱复用)来实现复用，其次才考虑使用继承关系(白箱复用)来实现。
    public static void main(String[] args) {
        ProduceDAO produceDAO = new ProduceDAO();
        produceDAO.setDbConnection(new MySQLConnection());
        produceDAO.addProduce();
    }
}
