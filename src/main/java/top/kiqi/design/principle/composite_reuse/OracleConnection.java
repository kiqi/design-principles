package top.kiqi.design.principle.composite_reuse;

public class OracleConnection extends DBConnection{
    @Override
    public String getConnection() {
        return "Oracle connection!";
    }
}
