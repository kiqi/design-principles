package top.kiqi.design.principle.composite_reuse;

public class ProduceDAO {
    DBConnection dbConnection;

    public void setDbConnection(DBConnection dbConnection){
        this.dbConnection = dbConnection;
    };

    public void addProduce(){
        String conn = dbConnection.getConnection();
        System.out.println("获取数据库链接，" + conn);
    }

}
