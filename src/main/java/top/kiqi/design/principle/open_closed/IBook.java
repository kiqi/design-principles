package top.kiqi.design.principle.open_closed;

public interface IBook {
    String getName();
    int getPrice();
    String getAuthor();

    String toString();
}
