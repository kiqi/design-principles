package top.kiqi.design.principle.open_closed;

public class ComputerBook implements IComputerBook{
    private NovelBook novelBook;
    private String scope;

    public ComputerBook(String name, int price, String author, String scope) {
        this.novelBook = new NovelBook(name, price, author);
        this.scope = scope;
    }

    @Override
    public String getName() {
        return novelBook.getName();
    }

    @Override
    public int getPrice() {
        return novelBook.getPrice();
    }

    @Override
    public String getAuthor() {
        return novelBook.getAuthor();
    }

    @Override
    public String getScope() {
        return this.scope;
    }

    @Override
    public String toString() {
        return "ComputerBook{" +
                "name='" + novelBook.getName() + '\'' +
                ", price=" + novelBook.getPrice() +
                ", author='" + novelBook.getAuthor() + '\'' +
                ", scope='" + scope + '\'' +
                '}';
    }
}
