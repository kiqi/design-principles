package top.kiqi.design.principle.open_closed;

public interface IComputerBook extends IBook{
    String getScope();
}
