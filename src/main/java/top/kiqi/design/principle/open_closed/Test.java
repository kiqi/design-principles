package top.kiqi.design.principle.open_closed;

public class Test {
    // 开闭原则 - 一个软件实体如类、模块和函数应该对扩展开放，对修改关闭。
    public static void main(String[] args) {
        IBook iBook = new NovelBook("天龙八部",17,"金庸");
        System.out.println(iBook);

        IComputerBook iComputerBook = new ComputerBook("Think in Java",54,"Bruce Eckel","99");
        System.out.println(iComputerBook);
    }
}
