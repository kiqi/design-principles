package top.kiqi.design.principle.liskov_substitution;

public class Rifle extends AbstractGun {
    @Override
    void shunt() {
        System.out.println("Rifle shunt!");
    }

    @Override
    void show() {
        System.out.println("Rifle show.");
    }
}
