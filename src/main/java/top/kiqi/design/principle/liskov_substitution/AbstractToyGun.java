package top.kiqi.design.principle.liskov_substitution;

public abstract class AbstractToyGun {
    AbstractGun abstractGun;

    abstract void show();
}
