package top.kiqi.design.principle.liskov_substitution.method_param;

import java.util.HashMap;

public class Child extends Base{
    public void method(HashMap hashMap){
        System.out.println("child.method();");
    };
}
