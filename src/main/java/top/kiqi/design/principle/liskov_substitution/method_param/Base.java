package top.kiqi.design.principle.liskov_substitution.method_param;

import java.util.Map;

public class Base {
    public void method(Map map){
        System.out.println("base.method();");
    }
}
