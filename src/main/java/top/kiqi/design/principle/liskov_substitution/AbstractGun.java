package top.kiqi.design.principle.liskov_substitution;

public abstract class AbstractGun {
    abstract void shunt();
    abstract void show();
}
