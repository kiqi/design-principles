package top.kiqi.design.principle.liskov_substitution;

public class ToyGun extends AbstractToyGun{
    @Override
    void show() {
        System.out.println("ToyGun show.");
    }
}
