package top.kiqi.design.principle.liskov_substitution;

public class Test {
    // 里氏替换原则 - 当特殊子类无法实现父类的方法时，采用关联委托关系(组合)，声音、形状等委托给AbstractGun处理，其他玩具枪相关的特性由AbstractToyGun进行拓展
    public static void main(String[] args) {
        Soldier soldier = new Soldier();

        soldier.setAbstractGun(new Handgun());
        soldier.killEnemy();

        soldier.setAbstractGun(new Rifle());
        soldier.killEnemy();

        new ToyGun().show();
    }
}
