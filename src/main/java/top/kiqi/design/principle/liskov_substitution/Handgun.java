package top.kiqi.design.principle.liskov_substitution;

public class Handgun extends AbstractGun{
    @Override
    void shunt() {
        System.out.println("HandGun shunt!");
    }

    @Override
    void show() {
        System.out.println("HandGun show.");
    }
}
