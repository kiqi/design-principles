package top.kiqi.design.principle.liskov_substitution.method_param;

import java.util.HashMap;

public class Test {
    // 里氏替换原则 - 子类重载父类方法时，要有不小于父类的前置条件
    public static void main(String[] args) {
        // 当子类前置条件小于父类前置条件时，根据重载的特性，对同一个对象，子类引用和父类引用会有不同的实现。
        Child child = new Child();
        Base base = child;
        base.method(new HashMap());
        child.method(new HashMap());
    }
}
