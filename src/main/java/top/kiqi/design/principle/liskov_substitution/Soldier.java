package top.kiqi.design.principle.liskov_substitution;

public class Soldier {
    private AbstractGun abstractGun;

    public Soldier() {
    }

    public void setAbstractGun(AbstractGun abstractGun) {
        this.abstractGun = abstractGun;
    }

    public void killEnemy(){
        abstractGun.shunt();
    }
}
