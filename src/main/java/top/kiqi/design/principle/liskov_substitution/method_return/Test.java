package top.kiqi.design.principle.liskov_substitution.method_return;

public class Test {
    // 子类实现父类方法时，要有不大于父类的后置条件(否则，IDE会检查不通过)
    public static void main(String[] args) {
        Base child = new Child();
        System.out.println(child.method());
    }
}
