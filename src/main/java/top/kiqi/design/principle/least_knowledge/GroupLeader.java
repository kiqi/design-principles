package top.kiqi.design.principle.least_knowledge;

import java.util.List;

public class GroupLeader {
    private List<Girl> girlList;

    public GroupLeader(List<Girl> girlList) {
        this.girlList = girlList;
    }

    public void countList(){
        System.out.println(girlList.size());
    };
}
