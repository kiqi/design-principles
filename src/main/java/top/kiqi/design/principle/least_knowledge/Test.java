package top.kiqi.design.principle.least_knowledge;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        // 迪米特法则：只与朋友类(成员对象，方法入参，方法返回值)交流
        Teacher teacher = new Teacher();

        List<Girl> girlList = new ArrayList<>();
        girlList.add(new Girl());
        GroupLeader groupLeader = new GroupLeader(girlList);

        teacher.command(groupLeader);

        // 迪米特法则(最小知道原则)：对于依赖的类知道的最少，尽量少的暴露public接口(被调用方将多个逻辑合并成一个接口对外提供服务)
        System.out.println(new Wizard().login());
    }
}
