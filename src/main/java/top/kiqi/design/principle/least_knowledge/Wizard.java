package top.kiqi.design.principle.least_knowledge;

public class Wizard {
    private boolean first(){
        return true;
    };
    private boolean then(){
        return true;
    };
    private boolean end(){
        return true;
    };

    public boolean login(){
        if (!first()) return false;
        if (!then()) return false;
        return end();
    }
}
