package top.kiqi.design.principle.single_responsibility;

// 用户行为类
public interface IUserBiz {
    boolean changePassword(IUserBO iUserBO, String password);
    void showUser(IUserBO iUserBO);
}
