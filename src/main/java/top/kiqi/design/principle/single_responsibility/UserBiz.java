package top.kiqi.design.principle.single_responsibility;

public class UserBiz implements IUserBiz{
    @Override
    public boolean changePassword(IUserBO iUserBO, String password) {
        iUserBO.setPassword(password);
        return true;
    }

    @Override
    public void showUser(IUserBO iUserBO) {
        System.out.println(iUserBO.toString());
    }
}
