package top.kiqi.design.principle.single_responsibility;

// 用户属性类
public interface IUserBO {
    void setUserID(String userID);
    String getUserID();
    void setPassword(String password);
    String getPassword();
    void setUserName(String userName);
    String getUserName();
}
