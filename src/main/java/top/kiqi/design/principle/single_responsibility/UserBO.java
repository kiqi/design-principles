package top.kiqi.design.principle.single_responsibility;

public class UserBO implements IUserBO{

    private String userID;
    private String password;
    private String userName;

    public UserBO(String userID, String password, String userName) {
        this.userID = userID;
        this.password = password;
        this.userName = userName;
    }

    @Override
    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String getUserID() {
        return userID;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "UserBO{" +
                "userID='" + userID + '\'' +
                ", password='" + password + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
