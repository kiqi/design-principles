package top.kiqi.design.principle.single_responsibility;

public class Test {
    // 单一职责原则： 一个类应该有且仅有一个引起它变化的原因，否则类应该被拆分 - 可降低变更引起的风险，修改只影响对应的功能，不会影响其他功能
    public static void main(String[] args) {
        IUserBO iUserBO = new UserBO("kiqi","20132014","kiqi");
        IUserBiz iUserBiz = new UserBiz();

        iUserBiz.showUser(iUserBO);

        iUserBiz.changePassword(iUserBO,"1314520");
        iUserBiz.showUser(iUserBO);
    }
}
