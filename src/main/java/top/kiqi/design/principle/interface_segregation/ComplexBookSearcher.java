package top.kiqi.design.principle.interface_segregation;

import java.util.Map;

public class ComplexBookSearcher implements IComplexBookSearcher{

    ISimpleBookSearcher iSimpleBookSearcher;

    public ComplexBookSearcher(ISimpleBookSearcher iSimpleBookSearcher) {
        this.iSimpleBookSearcher = iSimpleBookSearcher;
    }

    @Override
    public void searchByAuthor() {
        iSimpleBookSearcher.searchByAuthor();
    }

    @Override
    public void searchByTitle() {
        iSimpleBookSearcher.searchByTitle();
    }

    @Override
    public void searchByName() {
        iSimpleBookSearcher.searchByName();
    }

    @Override
    public void complexSearch(Map map) {
        System.out.println("complex search!");
    }
}
