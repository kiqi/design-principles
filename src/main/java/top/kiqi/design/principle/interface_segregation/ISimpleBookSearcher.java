package top.kiqi.design.principle.interface_segregation;

public interface ISimpleBookSearcher {
    void searchByAuthor();
    void searchByTitle();
    void searchByName();
}
