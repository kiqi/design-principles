package top.kiqi.design.principle.interface_segregation;

import java.util.HashMap;

public class Test {
    // 接口隔离原则 - 接口尽量小的特殊案例，当需要避免提供额外的功能时可进行接口拆分。
    public static void main(String[] args) {
        // 低权限用户，只能定向查询
        ISimpleBookSearcher iSimpleBookSearcher = new SimpleBookSearcher();
        iSimpleBookSearcher.searchByName();
        iSimpleBookSearcher.searchByTitle();
        iSimpleBookSearcher.searchByAuthor();

        // 管理权限用户，支持混合查询功能
        IComplexBookSearcher complexBookSearcher = new ComplexBookSearcher(new SimpleBookSearcher());
        complexBookSearcher.complexSearch(new HashMap());
        complexBookSearcher.searchByName();
        complexBookSearcher.searchByTitle();
        complexBookSearcher.searchByAuthor();
    }
}
