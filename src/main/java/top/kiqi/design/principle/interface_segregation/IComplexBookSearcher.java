package top.kiqi.design.principle.interface_segregation;

import java.util.Map;

public interface IComplexBookSearcher extends ISimpleBookSearcher{
    void complexSearch(Map map);
}
