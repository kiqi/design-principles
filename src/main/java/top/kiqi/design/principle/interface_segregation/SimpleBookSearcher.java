package top.kiqi.design.principle.interface_segregation;

public class SimpleBookSearcher implements ISimpleBookSearcher{

    public SimpleBookSearcher() {
    }

    @Override
    public void searchByAuthor() {
        System.out.println("search by author!");
    }

    @Override
    public void searchByTitle() {
        System.out.println("search by title!");
    }

    @Override
    public void searchByName() {
        System.out.println("search by name!");
    }
}
