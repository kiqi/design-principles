package top.kiqi.design.principle.dependence_inversion;

public class BWM implements ICar {
    @Override
    public void run() {
        System.out.println("run the BWM!");
    }
}
