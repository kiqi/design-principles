package top.kiqi.design.principle.dependence_inversion;

public class Benz implements ICar{
    @Override
    public void run() {
        System.out.println("run the Benz!");
    }
}
