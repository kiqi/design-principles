package top.kiqi.design.principle.dependence_inversion;

public class Test {
    // 面向接口编程，依赖于抽象而不依赖于具体。 --- 三种依赖注入方法
    public static void main(String[] args) {
        // 接口依赖
        Driver driver = new Driver();
        driver.drive(new Benz());
        driver.drive(new BWM());

        // 构造方法依赖
        Driver driver1 = new Driver(new Benz());
        driver1.drive();

        // setter方法注入依赖
        driver1.setiCar(new BWM());
        driver1.drive();
    }
}
