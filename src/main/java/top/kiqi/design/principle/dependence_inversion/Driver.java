package top.kiqi.design.principle.dependence_inversion;

public class Driver implements IDriver{

    public Driver() {
    }

    // 接口依赖
    @Override
    public void drive(ICar iCar) {
        iCar.run();
    }

    // 构造方法依赖
    private ICar iCar;

    public Driver(ICar iCar) {
        this.iCar = iCar;
    }

    public void drive() {
        iCar.run();
    }

    // setter方法注入依赖
    public void setiCar(ICar iCar) {
        this.iCar = iCar;
    }
}
