package top.kiqi.design.principle.dependence_inversion;

public interface ICar {
    void run();
}
