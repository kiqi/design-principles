package top.kiqi.design.principle.dependence_inversion;

public interface IDriver {
    void drive(ICar iCar);
}
