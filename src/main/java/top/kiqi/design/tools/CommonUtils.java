package top.kiqi.design.tools;

import com.mysql.cj.jdbc.MysqlDataSource;
import top.kiqi.design.pattern.creational.factory.factory_pool.Pool;

import javax.sql.DataSource;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class CommonUtils {
    public static String simpleFormatDate(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY/MM/dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    public static DataSource getDataSource(String propertiesPath){
        Properties properties = CommonUtils.loadProperties(propertiesPath);
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL((String)properties.get("url"));
        dataSource.setUser((String)properties.get("username"));
        dataSource.setPassword((String)properties.get("password"));
        return dataSource;
    }

    public static Properties loadProperties(String propertiesName) {
        Properties properties = null;
        try {
            InputStream is = Pool.class.getClassLoader().getResourceAsStream(propertiesName);
            properties = new Properties();
            properties.load(is);
        }catch (Exception e){
            e.printStackTrace();
        }
        return properties;
    }

    public static String toUpperFirstCase(String str) {
        char [] chars = str.toCharArray();
        chars[0] -= 32;
        return String.valueOf(chars);
    }

    public static String toLowerFirstCase(String str) {
        char [] chars = str.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }
}
