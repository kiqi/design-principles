package top.kiqi.design.pattern.behavioral.visitor.general;

public interface IElement {
    void accept(IVisitor visitor);
}
