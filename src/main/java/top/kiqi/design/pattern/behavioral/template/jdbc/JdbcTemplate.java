package top.kiqi.design.pattern.behavioral.template.jdbc;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class JdbcTemplate<T> {
    private DataSource dataSource;

    JdbcTemplate(DataSource dataSource){
        this.dataSource = dataSource;
    }

    public final List<T> executeQuery(String sql, RowMapper<T> rowMapper, Object[] values){
        try {
            // 1. 获取链接
            Connection conn = this.getConnection();

            // 2. 创建语句集
            PreparedStatement pstm = this.getPreparedStatement(conn, sql);

            // 3. 执行语句集
            ResultSet rs = this.executeQuery(pstm, values);

            // 4. 处理结果集
            List<T> result = this.parseResultSet(rs, rowMapper);

            // 5. 关闭结果集
            rs.close();

            // 6. 关闭语句集
            pstm.close();

            // 7. 关闭链接
            conn.close();

            return result;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public final int executeUpdate(String sql){
        try {
            // 1. 获取链接
            Connection conn = this.getConnection();

            // 2. 创建语句集
            PreparedStatement pstm = this.getPreparedStatement(conn, sql);

            // 3. 执行语句集
            int result = this.executeUpdate(pstm);

            // 6. 关闭语句集
            pstm.close();

            // 7. 关闭链接
            conn.close();

            return result;
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

    // 将查询结果映射到list中(ORM)
    protected List<T> parseResultSet(ResultSet rs, RowMapper<T> rowMapper) throws SQLException{
        List<T> result = new ArrayList<>();
        int rowNum = 0;
        while (rs.next()){
            result.add(rowMapper.mapRow(rs,rowNum++));
        }
        return result;
    }

    // 执行查询操作
    protected ResultSet executeQuery(PreparedStatement pstm, Object[] values) throws SQLException{
        if(values != null) {
            for (int i = 0; i < values.length; i++) {
                pstm.setObject(i, values[i]);
            }
        }
        return pstm.executeQuery();
    }

    // 执行更新操作
    protected int executeUpdate(PreparedStatement pstm) throws SQLException{
        return pstm.executeUpdate();
    }


    // 获取语句集
    protected PreparedStatement getPreparedStatement(Connection conn, String sql) throws SQLException{
        return conn.prepareStatement(sql);
    };

    // 获取链接
    protected Connection getConnection() throws SQLException{
        return this.dataSource.getConnection();
    };
}
