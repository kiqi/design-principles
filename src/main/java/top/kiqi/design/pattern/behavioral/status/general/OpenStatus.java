package top.kiqi.design.pattern.behavioral.status.general;

public class OpenStatus extends AbstractStatus{
    public OpenStatus(Context context) {
        super(context);
    }

    @Override
    public void open() {
        System.out.println("command open - keep open");
    }

    @Override
    public void close() {
        System.out.println("command close - close the door");
        this.context.setStatus(this.context.STOP_CLOSE);
    }
}
