package top.kiqi.design.pattern.behavioral.observer.community;

public class IUser {
    private String userId;
    private Context context;

    public IUser(String userId, Context context){
        this.userId = userId;
        this.context = context;
    }

    public void publish(String message){
        context.publish(userId,message);
    }

    public void subscribe(String userId, String message) {
        System.out.println(this.userId + "： message from " + userId + " - " + message);
    }
}
