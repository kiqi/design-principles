package top.kiqi.design.pattern.behavioral.interpreter.tools;

import top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.SymbolExpression;
import top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.symbol.*;

public class OperatorUtil {
    public static boolean isOperator(char c){
        return c == '+' || c == '-' || c == '*' | c == '/';
    }

    public static SymbolExpression getSymbolExpression(char symbol, int baseLevel) {
        if (symbol == '+') {
            return new AddExpression(baseLevel);
        } else if (symbol == '-') {
            return new SubExpression(baseLevel);
        } else if (symbol == '*') {
            return new MultiExpression(baseLevel);
        } else if (symbol == '/') {
            return new DivExpression(baseLevel);
        }
        return null;
    }
}
