package top.kiqi.design.pattern.behavioral.command.actions;

// 命令抽象，存放命令的具体实现逻辑
public interface ICommandAction {
    void execute();
}
