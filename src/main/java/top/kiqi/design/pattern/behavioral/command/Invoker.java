package top.kiqi.design.pattern.behavioral.command;

import top.kiqi.design.pattern.behavioral.command.actions.*;

// 负责命令的执行，其中可以增加一些控制逻辑(如：取消，暂停等)
public class Invoker {
    public void actios(ICommandAction command){
        command.execute();
    }

    // 命令列表，作为用户请求命令的入口
    public static class Commands{
        public final static ICommandAction PLAY = new PlayAction();
        public final static ICommandAction STOP = new StopAction();
        public final static ICommandAction TURN_DOWN = new TurnDownVoicesAction();
        public final static ICommandAction TURN_UP = new TurnUpVoicesAction();
    }
}
