package top.kiqi.design.pattern.behavioral.mediator.general;

public class ConcreteColleageA extends Colleage{
    protected ConcreteColleageA(Mediator mediator) {
        super(mediator);
    }

    // 自有方法，由类自己实现
    public void selfMethodA(){
        System.out.println("ColleageA - Do method A!");
    }

    // 依赖方法，需要调用其他类实现(在中介者模式中，通知中介类，由中介类去协调处理)
    public void dependenceMethodB(){
        System.out.print("ColleageA call method B-> ");
        mediator.doMethodB();
    }
}
