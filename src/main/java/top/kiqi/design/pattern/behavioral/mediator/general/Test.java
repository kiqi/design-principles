package top.kiqi.design.pattern.behavioral.mediator.general;

// 功能不同的同事，由中介者实现消息转发 ---》 使同事类之间解耦(网状 -》 星状) --- 实例：服务治理
// 使同事类的改动范围限制在自身和中介类中， 弊端：中介类可能会很复杂。
public class Test {
    public static void main(String[] args) {
        Mediator mediator = new Mediator();
        ConcreteColleageA concreteColleageA = new ConcreteColleageA(mediator);
        ConcreteColleageB concreteColleageB = new ConcreteColleageB(mediator);

        concreteColleageA.dependenceMethodB();  // ColleageA call method B-> Mediator call -> ColleageB - Do method B!
        concreteColleageA.selfMethodA();        // ColleageA - Do method A!

        concreteColleageB.dependenceMethodA();  // ColleageB call method A-> Mediator call -> ColleageA - Do method A!
        concreteColleageB.selfMethodB();        // ColleageB - Do method B!
    }
}
