package top.kiqi.design.pattern.behavioral.interpreter.calculate.expression;

// 非终结符表达式，操作符
public abstract class SymbolExpression implements IExpression {
    protected int level;

    public SymbolExpression(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
