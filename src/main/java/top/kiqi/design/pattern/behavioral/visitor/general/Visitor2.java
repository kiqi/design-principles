package top.kiqi.design.pattern.behavioral.visitor.general;

public class Visitor2 implements IVisitor{
    public void visit(ElementA element) {
        System.out.println(element.getId() + " - kpi:" + element.getCore() / element.getLevel() );
    }

    public void visit(ElementB element) {
        System.out.println(element.getId() + " - kpi:" + element.getSpend() / element.getLevel());
    }
}
