package top.kiqi.design.pattern.behavioral.observer.general;

public interface ISubject<E> {
    boolean register(IObserver<E> observer);

    boolean deregister(IObserver<E> observer);

    void notify(E event);
}
