package top.kiqi.design.pattern.behavioral.template.model;

public class NeedStep3Model extends AbstractModel{

    private boolean needStep3 = true;

    @Override
    protected void step1() {
        System.out.println("step1");
    }

    @Override
    protected void step2() {
        System.out.println("step2");
    }

    @Override
    protected void step3() {
        System.out.println("step3");
    }

    @Override
    protected boolean needStep3(){
        return needStep3;
    }

    public void setNeedStep3(boolean needStep3){
        this.needStep3 = needStep3;
    }
}
