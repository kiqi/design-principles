package top.kiqi.design.pattern.behavioral.visitor.general;

import java.util.ArrayList;

public class ElementContainer {
    private ArrayList<IElement> elements = new ArrayList<>();

    public void show(IVisitor visitor){
        for(IElement element : elements){
            element.accept(visitor);
        }
    }

    public void addElement(IElement element){
        elements.add(element);
    }
}
