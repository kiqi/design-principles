package top.kiqi.design.pattern.behavioral.visitor.general;

public class Test {
    public static void main(String[] args) {
        ElementContainer container = new ElementContainer();
        System.out.println("visitor1: ");
        container.show(new Visitor1());
        System.out.println("visitor2: ");
        container.show(new Visitor2());
    }
}
