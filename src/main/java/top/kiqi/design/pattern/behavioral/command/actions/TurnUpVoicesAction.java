package top.kiqi.design.pattern.behavioral.command.actions;

public class TurnUpVoicesAction implements ICommandAction {
    @Override
    public void execute() {
        System.out.println("Turn Up the voices!");
    }
}
