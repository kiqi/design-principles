package top.kiqi.design.pattern.behavioral.mediator.general;

public class ConcreteColleageB extends Colleage{
    protected ConcreteColleageB(Mediator mediator) {
        super(mediator);
    }

    // 自有方法，由类自己实现
    public void selfMethodB(){
        System.out.println("ColleageB - Do method B!");
    }

    // 依赖方法，需要调用其他类实现(在中介者模式中，通知中介类，由中介类去协调处理)
    public void dependenceMethodA(){
        System.out.print("ColleageB call method A-> ");
        mediator.doMethodA();
    }
}
