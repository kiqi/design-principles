package top.kiqi.design.pattern.behavioral.momente.white_box;

public class Originator {
    private String state;

    public Originator(String state){
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memonte createMonte(){
        return new Memonte(this.state);
    }

    public void restoreMemonte(Memonte memonte){
        this.state = memonte.getState();
    };

    public void show(){
        System.out.println("State = " + this.state);
    }
}
