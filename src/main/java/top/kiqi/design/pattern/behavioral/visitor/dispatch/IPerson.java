package top.kiqi.design.pattern.behavioral.visitor.dispatch;

public interface IPerson {
    String getSex();
}
