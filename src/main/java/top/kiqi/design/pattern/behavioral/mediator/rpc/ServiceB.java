package top.kiqi.design.pattern.behavioral.mediator.rpc;

// 服务B 提供展示功能，同时调用服务A 执行记录
public class ServiceB extends AbstractService{
    private Mediator mediator;

    public ServiceB(){
    }

    @Override
    public void register() {
        mediator = Mediator.getInstance();
        mediator.register("ServiceB",this);
    }

    @Override
    protected Object receive(String methodId, Object[] params) {
        if("show".equals(methodId)){
            show(params);
        }
        return null;
    }

    private void show(Object[] params){
        System.out.println("ServiceB - 执行ServiceB.show功能");
        System.out.println("ServiceB - 申请serviceA.record()方法");
        mediator.relay("ServiceA","record",params);
    }
}
