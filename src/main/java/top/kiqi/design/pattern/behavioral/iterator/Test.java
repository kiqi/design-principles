package top.kiqi.design.pattern.behavioral.iterator;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Test {
    public static void main(String[] args) throws RunnerException {
        // ArrayList的遍历方式，没有明显的性能区别，时间复杂度均为O(n);
        // LinkedList的遍历方式，foreach和iterator时间复杂度为O(n)，而for()遍历时间复杂度为O(n^2);

        // 启动基准测试
        Options opt = new OptionsBuilder()
                .include(ListTest.class.getSimpleName()) // 要导入的测试类
                .output("jmh-lista.log") // 输出测试结果的文件
                .build();
        new Runner(opt).run(); // 执行测试
    }
}
