package top.kiqi.design.pattern.behavioral.interpreter.calculate.expression;

// 终结符表达式，操作数
public class VarExpression implements IExpression {
    private int value;

    public VarExpression(int value) {
        this.value = value;
    }

    @Override
    public int interpret(IExpression... params) {
        return value;
    }
}
