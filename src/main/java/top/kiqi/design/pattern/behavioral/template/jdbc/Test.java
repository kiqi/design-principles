package top.kiqi.design.pattern.behavioral.template.jdbc;

import com.mysql.cj.jdbc.MysqlDataSource;
import top.kiqi.design.tools.CommonUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class Test {
    public static void main(String[] args) throws SQLException {

        createTable();

        //insertTest(new Member("kiqi2","123456",18,"jx"));

        //updateTest(new Member("kiqi2","123456",18,"ncu"));
    }

    // 更新数据库
    public static void updateTest(Member member){
        DataSource dataSource = CommonUtils.getDataSource("connection.properties");
        MemberDao memberDao = new MemberDao(dataSource);
        System.out.println(memberDao.update(member));
    }

    // 插入数据库
    public static void insertTest(Member member){
        DataSource dataSource = CommonUtils.getDataSource("connection.properties");
        MemberDao memberDao = new MemberDao(dataSource);
        System.out.println(memberDao.insert(member));
    }

    // 查询
    public static void seleteAllTest(){
        DataSource dataSource = CommonUtils.getDataSource("connection.properties");
        MemberDao memberDao = new MemberDao(dataSource);
        List<Member> list = memberDao.selectAll();

        for(Member member : list){
            System.out.println(member.toString());
        }
    }

    public static void createTable() throws SQLException{
        DataSource dataSource = CommonUtils.getDataSource("connection.properties");
        Connection conn = dataSource.getConnection();
        PreparedStatement pstm = conn.prepareStatement("CREATE TABLE `kiqi`(" +
                        "`id` INT UNSIGNED AUTO_INCREMENT," +
                        "`username` VARCHAR(24) NOT NULL," +
                        "`password` VARCHAR(24) NOT NULL," +
                        "`age` INT NOT NULL," +
                        "`addr` VARCHAR(100)" +
                        ",PRIMARY KEY(`id`))ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        int i = pstm.executeUpdate();
        System.out.println("创建成功: " + i);
    }
}
