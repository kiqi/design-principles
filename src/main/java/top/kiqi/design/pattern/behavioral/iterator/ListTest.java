package top.kiqi.design.pattern.behavioral.iterator;

import org.openjdk.jmh.annotations.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

// ArrayList的遍历方式，没有明显的性能区别，时间复杂度均为O(n);
// LinkedList的遍历方式，foreach和iterator时间复杂度为O(n)，而for()遍历时间复杂度为O(n^2);
@BenchmarkMode(Mode.Throughput) // 测试类型：吞吐量
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS) // 预热 5 轮，每次 1s
@Measurement(iterations = 5, time = 2, timeUnit = TimeUnit.SECONDS) // 测试 5 轮，每次 2s
@Fork(1) // fork 1 个线程
@State(Scope.Thread) // 每个测试线程一个实例
public class ListTest {
    private static List<String> list = null;
    static {
        list = new ArrayList<>();
        for(int i = 0;i < 50;i++){
            list.add(UUID.randomUUID().toString());
        }
    }

    // 1. foreach遍历
    @Benchmark
    public static void foreachList(){
        for (String j : list) {
        }
    }

//    // 2. iterator遍历
//    @Benchmark
//    public static void iteratorList(){
//        String j;
//        Iterator<String> iterator = list.iterator();
//        while (iterator.hasNext()){
//            j = iterator.next();
//        }
//    }
//
//    // 3. for 递减遍历
//    @Benchmark
//    public static void forDescList(){
//        String j;
//        for(int i = list.size() - 1;i >= 0; i--){
//            j = list.get(i);
//        }
//    }
//
//    // 4. for 递增遍历
//    @Benchmark
//    public static void forAscList(){
//        String j;
//        int size = list.size();
//        for(int i = 0;i < size; i++){
//            j = list.get(i);
//        }
//    }

    // 5. for 递增遍历
    @Benchmark
    public static void foreachJava8(){
        list.forEach(item -> {

        });
    }
}
