package top.kiqi.design.pattern.behavioral.interpreter.calculate.expression;

public interface IExpression {
    int interpret(IExpression... params);
}
