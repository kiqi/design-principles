package top.kiqi.design.pattern.behavioral.template.jdbc;


import java.sql.ResultSet;

public interface RowMapper<T> {
    T mapRow(ResultSet rs, int rowNum);
}
