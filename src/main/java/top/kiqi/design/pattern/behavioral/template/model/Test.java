package top.kiqi.design.pattern.behavioral.template.model;

public class Test {
    // 模板方法模式 - 父类做出行为规划，子类实现行为细节。子类可以通过钩子方法对行为规则做出一定调整
    public static void main(String[] args) {
        NeedStep3Model needStep3Model = new NeedStep3Model();
        needStep3Model.execute();

        needStep3Model.setNeedStep3(false);
        needStep3Model.execute();


    }
}
