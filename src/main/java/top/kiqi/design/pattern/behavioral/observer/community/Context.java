package top.kiqi.design.pattern.behavioral.observer.community;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

// 职责1：维护订阅关系。
// 职责2：接受到事件消息时，完成分发
public class Context {
    private ConcurrentHashMap<String, ConcurrentLinkedQueue<IUser>> subscribeMap = new ConcurrentHashMap<>();

    // 订阅
    public boolean subscribe(String publishUserId, IUser subscribeUser){
        if(!subscribeMap.containsKey(publishUserId)) {
            subscribeMap.putIfAbsent(publishUserId, new ConcurrentLinkedQueue<IUser>());
        }
        return subscribeMap.get(publishUserId).add(subscribeUser);
    }

    public boolean unsubscribe(String publishUserId, IUser subscribeUser){
        return subscribeMap.get(publishUserId).remove(subscribeUser);
    }

    // 分发
    public void publish(String userId, String message) {
        IUser[] iUsers = subscribeMap.get(userId).toArray(new IUser[subscribeMap.get(userId).size()]);
        for(IUser iUser : iUsers){
            iUser.subscribe(userId, message);
        }
    }
}
