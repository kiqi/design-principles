package top.kiqi.design.pattern.behavioral.status.general;

public class RunStatus extends AbstractStatus{
    public RunStatus(Context context) {
        super(context);
    }

    @Override
    public void run() {
        System.out.println("command run - keep run!");
    }

    @Override
    public void stop() {
        System.out.println("command stop - stop run!");
        this.context.setStatus(this.context.STOP_CLOSE);
    }
}
