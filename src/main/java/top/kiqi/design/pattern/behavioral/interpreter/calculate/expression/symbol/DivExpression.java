package top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.symbol;

import top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.IExpression;
import top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.SymbolExpression;

public class DivExpression extends SymbolExpression {

    public DivExpression(int baseLevel) {
        super(baseLevel + 1);
    }


    @Override
    public int interpret(IExpression... params) {
        return params[1].interpret() / params[0].interpret();
    }
}
