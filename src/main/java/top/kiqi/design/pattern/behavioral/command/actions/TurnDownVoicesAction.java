package top.kiqi.design.pattern.behavioral.command.actions;

public class TurnDownVoicesAction implements ICommandAction {
    @Override
    public void execute() {
        System.out.println("Turn down the voices!");
    }
}
