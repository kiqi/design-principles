package top.kiqi.design.pattern.behavioral.status.mechine;

public class RunStatus extends AbstractStatus {
    @Override
    public String getStatusName() {
        return "RUN";
    }

    @Override
    public void run() {
        System.out.println("command run - keep run!");
    }

    @Override
    public void stop() {
        System.out.println("command stop - stop run!");
    }
}
