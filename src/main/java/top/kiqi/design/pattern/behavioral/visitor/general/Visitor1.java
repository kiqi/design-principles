package top.kiqi.design.pattern.behavioral.visitor.general;

public class Visitor1 implements IVisitor{
    public void visit(ElementA element) {
        System.out.println(element.getId() + " - core:" + element.getCore() );
    }

    public void visit(ElementB element) {
        System.out.println(element.getId() + " - spend:" + element.getSpend() );
    }
}
