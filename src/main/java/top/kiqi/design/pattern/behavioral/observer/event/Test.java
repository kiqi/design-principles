package top.kiqi.design.pattern.behavioral.observer.event;

import top.kiqi.design.pattern.behavioral.observer.event.handler.MouseEventListener;
import top.kiqi.design.pattern.behavioral.observer.event.handler.MouseHandler;

public class Test {
    public static void main(String[] args) {
        MouseHandler mouseHandler = new MouseHandler();
        MouseEventListener mouseEventListener = new MouseEventListener();
        mouseHandler.addListener("click", mouseEventListener);
        mouseHandler.addListener("move", mouseEventListener);

        mouseHandler.click();
        mouseHandler.move();
    }
}
