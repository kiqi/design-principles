package top.kiqi.design.pattern.behavioral.delegate;

import java.util.HashMap;
import java.util.Map;

public class Delegater{
    // 初始化策略
    private Map<String,Executable> executors = null;
    {
        executors = new HashMap<>();
        executors.put("A",new ExecutorA());
        executors.put("B",new ExecutorB());
    }

    public void execute(String command) {
       if(executors.containsKey(command)){
           // 根据策略选择委托人，将任务委托给执行人执行。
           executors.get(command).execute(command);
       }
    }
}
