package top.kiqi.design.pattern.behavioral.momente.white_box;

public class Caretaker {
    public Memonte getMemonte() {
        return memonte;
    }

    public void setMemonte(Memonte memonte) {
        this.memonte = memonte;
    }

    private Memonte memonte;
}