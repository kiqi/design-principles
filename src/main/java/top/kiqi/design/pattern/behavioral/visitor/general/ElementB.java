package top.kiqi.design.pattern.behavioral.visitor.general;

public class ElementB implements IElement{
    private int id = 0;
    private int spend = 0;
    private int level = 0;

    public ElementB() {
        this.id = (int)(Math.random() * 10001);
        this.spend = (int)(Math.random() * 101);
        this.level = (int)(Math.random() * 11);
    }

    public int getId() {
        return id;
    }

    public int getSpend() {
        return spend;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
