package top.kiqi.design.pattern.behavioral.chain;

public class LoginHandler extends Handler {
    @Override
    public void handlerRequest(MemberInfo memberInfo) {
        if(memberInfo.getLogin() == 0){
            login(memberInfo);
        }
        if(memberInfo.getLogin() == 1) {
            nextHandlerIfExist(memberInfo);
        }
    }

    private void login(MemberInfo memberInfo) {
        System.out.println(memberInfo.getUsername() + " login!");
        memberInfo.setLogin(1);
        if(Math.random() > 0.5){
            memberInfo.setAuth(1);
        }
    }
}
