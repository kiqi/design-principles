package top.kiqi.design.pattern.behavioral.visitor.general;

public class ElementA implements IElement{
    private int id = 0;
    private int core = 0;
    private int level = 0;

    public ElementA() {
        this.id = (int)(Math.random() * 10001);;
        this.core = (int)(Math.random() * 101);
        this.level = (int)(Math.random() * 11);
    }

    public int getId() {
        return id;
    }

    public int getCore() {
        return core;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
