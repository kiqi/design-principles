package top.kiqi.design.pattern.behavioral.visitor.dispatch;

public class Woman implements IPerson {
    private static final String SEX = "Woman";

    @Override
    public String getSex() {
        return SEX;
    }
}
