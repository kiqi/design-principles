package top.kiqi.design.pattern.behavioral.status.mechine;

public interface IStatus {

    void run();

    void stop();

    void open();

    void close();
}
