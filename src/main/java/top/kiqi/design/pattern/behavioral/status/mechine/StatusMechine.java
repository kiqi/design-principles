package top.kiqi.design.pattern.behavioral.status.mechine;

import java.util.HashMap;
import java.util.Map;

public class StatusMechine{
    // 行为类型
    protected final static String RUN_ACTION = "RUN";
    protected final static String STOP_ACTION = "STOP";
    protected final static String OPEN_ACTION = "OPEN";
    protected final static String CLOSE_ACTION = "CLOSE";

    // 状态类型
    protected final static AbstractStatus OPEN_STATUS = new OpenStatus();
    protected final static AbstractStatus STOP_CLOSE_STATUS = new StopStatus();
    protected final static AbstractStatus RUN_STATUS = new RunStatus();

    // 状态流转表
    protected static Map<String,AbstractStatus> transMap = new HashMap<>();

    // 执行状态转换
    public static AbstractStatus trans(AbstractStatus status, String actions) {
        String key = status.getStatusName() + actions;
        return transMap.getOrDefault(key, status);
    }

    // 状态转换过程注册方法
    protected static void registerStatusTrans(AbstractStatus status, String action, AbstractStatus nextStatus){
        transMap.put(status.getStatusName() + action, nextStatus);
    }

    // 将状态的转换注册到状态机中
    static {
        StatusMechine.registerStatusTrans(StatusMechine.RUN_STATUS, StatusMechine.STOP_ACTION, StatusMechine.STOP_CLOSE_STATUS);
        StatusMechine.registerStatusTrans(StatusMechine.STOP_CLOSE_STATUS, StatusMechine.RUN_ACTION, StatusMechine.RUN_STATUS);
        StatusMechine.registerStatusTrans(StatusMechine.STOP_CLOSE_STATUS, StatusMechine.OPEN_ACTION, StatusMechine.OPEN_STATUS);
        StatusMechine.registerStatusTrans(StatusMechine.OPEN_STATUS, StatusMechine.CLOSE_ACTION, StatusMechine.STOP_CLOSE_STATUS);
    }
}
