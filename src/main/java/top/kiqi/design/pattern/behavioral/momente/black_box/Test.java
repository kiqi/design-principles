package top.kiqi.design.pattern.behavioral.momente.black_box;

// 黑箱模式备忘录 - 采用双接口设计，客户端中获取到空接口对象，屏蔽了对象的功能操作
public class Test {
    public static void main(String[] args) {
        Originator originator = new Originator("Stop");  // 1. Stop

        // 创建备份
        Caretaker caretaker = new Caretaker();
        IMemonte memonte = originator.createMonte();            // IMemonte是空接口，IMemonte对客户端而言屏蔽了功能细节
        caretaker.setMemonte(memonte);

        // 更改状态
        originator.setState("Start");                           // 2. Start

        // 恢复备份
        originator.restoreMemonte(caretaker.getMemonte());      // 3. Stop
    }
}
