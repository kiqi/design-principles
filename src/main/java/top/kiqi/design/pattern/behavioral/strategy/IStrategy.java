package top.kiqi.design.pattern.behavioral.strategy;

public interface IStrategy {
    void algorithm();
}
