package top.kiqi.design.pattern.behavioral.status.general;

// 普通状态模式，在Status行为内部实现状态转换(违背单一职责原则)
public class Test {
    public static void main(String[] args) {
        Context context = new Context();  // 初始状态为 stop_close
        context.close();                  // 维持状态不变 stop_close
        context.open();                   // 跳转到open状态
        context.close();                  // 跳转到stop_close状态
        context.run();                    // 跳转到run状态
        context.close();                  // run状态下，无法执行close指令 - do nothing
        context.stop();                   // 跳转到stop_close状态
    }
}
