package top.kiqi.design.pattern.behavioral.delegate;

public interface Executable {
    void execute(String command);
}
