package top.kiqi.design.pattern.behavioral.observer.event.handler;

import top.kiqi.design.pattern.behavioral.observer.event.core.Event;
import top.kiqi.design.pattern.behavioral.observer.event.core.EventListener;

// 通过反射实现调用
public class MouseEventListener implements EventListener {
    public void onClick(Event event){
        System.out.println("MouseEventListener - 触发单击事件！");
    }


    public void onMove(Event event){
        System.out.println("MouseEventListener - 触发移动事件！");
    }
}
