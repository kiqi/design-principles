package top.kiqi.design.pattern.behavioral.delegate.mvc;

import top.kiqi.design.pattern.behavioral.template.jdbc.Member;
import top.kiqi.design.pattern.behavioral.template.jdbc.MemberDao;
import top.kiqi.design.tools.CommonUtils;

import javax.sql.DataSource;
import java.util.List;

public class MemberController {

    private DataSource dataSource = CommonUtils.getDataSource("connection.properties");

    private MemberDao memberDao = new MemberDao(dataSource);

    public void selectAll() {
        List<Member> list = memberDao.selectAll();

        for(Member member : list){
            System.out.println(member.toString());
        }
    }
}
