package top.kiqi.design.pattern.behavioral.chain;

public class MemberInfo {
    private String username = null;
    private String password = null;
    private int login = 0;   // - 0 - 未登录 1 - 已登录
    private int auth = 0;    // - 0 - 账号无权限 1 - 账号有权限

    public MemberInfo(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLogin() {
        return login;
    }

    public void setLogin(int login) {
        this.login = login;
    }

    public int getAuth() {
        return auth;
    }

    public void setAuth(int auth) {
        this.auth = auth;
    }
}
