package top.kiqi.design.pattern.behavioral.visitor.dispatch;

public class Dispatch {
    public static void main(String[] args) {
        // 方法重载，静态分派 -- 编译阶段即可确定调用方法
        Dispatch dispatch = new Dispatch();
        dispatch.print(100);
        dispatch.print("100");

        // 方法重写，动态分派 -- 只有运行过程中可确定调用方法
        IPerson person = dispatch.createPerson();
        person.getSex();
    }

    private IPerson createPerson(){
        if(Math.random() * 2 > 1){
            return new Woman();
        }else {
            return new Man();
        }
    }

    private void print(String str){
        System.out.println("Str is " + str);
    };

    private void print(Integer i){
        System.out.println("Num is " + i);
    }
}
