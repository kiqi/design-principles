package top.kiqi.design.pattern.behavioral.template.jdbc;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Tom.
 */
public class KiqiDao<T> extends JdbcTemplate {
    public KiqiDao(DataSource dataSource) {
        super(dataSource);
    }

    public List<T> selectAll(){
        String sql = "select * from kiqi";

        RowMapper rowMapper = (rs,i) ->{
            try {
                Member member = new Member();
                //字段过多，原型模式
                member.setUsername(rs.getString("username"));
                member.setPassword(rs.getString("password"));
                member.setAge(rs.getInt("age"));
                member.setAddr(rs.getString("addr"));
                return member;
            }catch (SQLException e){
                e.printStackTrace();
                return null;
            }
        };

        return super.executeQuery(sql, rowMapper,null);
    }
}
