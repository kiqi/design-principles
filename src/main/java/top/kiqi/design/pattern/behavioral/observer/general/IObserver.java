package top.kiqi.design.pattern.behavioral.observer.general;

public interface IObserver<E> {
    void update(E event);
}
