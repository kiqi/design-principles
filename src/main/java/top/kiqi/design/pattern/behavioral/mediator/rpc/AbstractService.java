package top.kiqi.design.pattern.behavioral.mediator.rpc;

public abstract class AbstractService {
    protected abstract void register();

    protected abstract Object receive(String methodId, Object[] params);
}
