package top.kiqi.design.pattern.behavioral.status.general;

public class StopStatus extends AbstractStatus{
    public StopStatus(Context context) {
        super(context);
    }

    @Override
    public void run() {
        System.out.println("command run - start run!");
        this.context.setStatus(this.context.RUN);
    }

    @Override
    public void stop() {
        System.out.println("command stop - keep stop_close!");
    }

    @Override
    public void open() {
        System.out.println("command open - open the door!");
        this.context.setStatus(this.context.OPEN);
    }

    @Override
    public void close() {
        System.out.println("command close - keep stop_close!");
    }
}
