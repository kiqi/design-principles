package top.kiqi.design.pattern.behavioral.observer.event.handler;

import top.kiqi.design.pattern.behavioral.observer.event.core.EventContext;

public class MouseHandler extends EventContext {
    public void click(){
        System.out.println("单击方法");
        this.trigger(MouseEventType.ON_CLICK);  // 事件发布。
    }

    public void move(){
        System.out.println("移动方法");
        this.trigger(MouseEventType.ON_MOVE);
    }
}
