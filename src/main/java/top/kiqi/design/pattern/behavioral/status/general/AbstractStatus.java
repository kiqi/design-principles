package top.kiqi.design.pattern.behavioral.status.general;

// 抽象状态对象，提供公共的默认实现(由于在状态内部进行状态流转，因此还需持有context上下文信息)
public abstract class AbstractStatus implements IStatus{

    protected Context context = null;

    protected AbstractStatus(Context context){
        this.context = context;
    }

    @Override
    public void run() {
        System.out.println("command run - Do nothing!");
    }

    @Override
    public void stop() {
        System.out.println("command stop - Do nothing!");
    }

    @Override
    public void open() {
        System.out.println("command open - Do nothing!");
    }

    @Override
    public void close() {
        System.out.println("command close - Do nothing!");
    }
}
