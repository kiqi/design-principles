package top.kiqi.design.pattern.behavioral.mediator.rpc;

import java.util.HashMap;
import java.util.Map;

// 中介者，维护所有的服务列表，同时负责转发服务请求
public class Mediator {
    // 服务管理者只能有一份，使用单例模式创建
    private final static Mediator mediator = new Mediator();

    private Mediator() {}

    public static Mediator getInstance(){
        return mediator;
    }

    // 维护所有的服务列表<serviceId, service实例>
    private Map<String, AbstractService> serviceMap = new HashMap<>();

    // 服务需要注册到Mediator中，才能被其他服务调用
    public final void register(String serviceId, AbstractService service){
        serviceMap.put(serviceId, service);
    }

    // 根据serviceId查找对应的服务对象，并将调用方法名和参数传递过去
    public Object relay(String serviceId, String method, Object[] params){
        System.out.println("Mediator.relay() - " + serviceId + "." + method + "()");
        if (serviceMap.containsKey(serviceId)) {
            AbstractService service = serviceMap.get(serviceId);
            return service.receive(method, params);
        }
        return null;
    }
}
