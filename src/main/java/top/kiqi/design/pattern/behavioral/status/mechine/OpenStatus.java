package top.kiqi.design.pattern.behavioral.status.mechine;

public class OpenStatus extends AbstractStatus {
    @Override
    public String getStatusName() {
        return "OPEN";
    }

    @Override
    public void open() {
        System.out.println("command open - keep open");
    }

    @Override
    public void close() {
        System.out.println("command close - close the door");
    }
}
