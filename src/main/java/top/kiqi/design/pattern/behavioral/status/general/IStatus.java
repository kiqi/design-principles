package top.kiqi.design.pattern.behavioral.status.general;

public interface IStatus {
    void run();

    void stop();

    void open();

    void close();
}
