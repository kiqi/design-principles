package top.kiqi.design.pattern.behavioral.delegate.mvc;

public class Test {
    public static void main(String[] args) {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        dispatcherServlet.doDispatch("/selectAll");
    }
}
