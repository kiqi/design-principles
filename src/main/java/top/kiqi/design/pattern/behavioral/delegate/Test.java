package top.kiqi.design.pattern.behavioral.delegate;

// 委派模式 - 调用方将执行参数传递给委托者，委托者根据参数(策略)，选择具体的执行人执行任务
// 在大多情况下(单一委派除外)，委派模式和策略模式是一同使用的。
// 委派的定义侧重于：持有被委派对象的引用，根据合适的策略将任务委派给被委派对象执行(调度)
// 策略的定义侧重于：根据用户参数或由用户自行决定选取何种策略(选择)
public class Test {
    public static void main(String[] args) {
        Delegater delegater = new Delegater();
        delegater.execute("A");
        delegater.execute("B");
    }
}
