package top.kiqi.design.pattern.behavioral.mediator.chatRoom;

// 中介者作为平台，为所有同事对象提供平台的功能。(同事只需和中介者交互)
public class Test {
    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom();
        User userA = new User("kiqi");
        User userB = new User("xike");

        userA.chat(chatRoom,"你啥时候放假？");
        userB.chat(chatRoom,"不知道啊，下周四吧");
        userA.chat(chatRoom,"这样吗，好早啊，我这都没下通知！！！");
        userB.chat(chatRoom,"哦，这样子的老板，炒了吧，我养你！");

        chatRoom.show();
    }
}
