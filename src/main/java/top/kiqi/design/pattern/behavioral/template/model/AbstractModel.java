package top.kiqi.design.pattern.behavioral.template.model;

public abstract class AbstractModel {
    protected abstract void step1();
    protected abstract void step2();
    protected abstract void step3();

    protected boolean needStep3(){
        return true;
    }

    public final void execute(){
        step1();
        step2();
        if(needStep3()){
            step3();
        }
    }
}
