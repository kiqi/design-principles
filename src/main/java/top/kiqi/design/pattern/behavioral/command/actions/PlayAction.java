package top.kiqi.design.pattern.behavioral.command.actions;

public class PlayAction implements ICommandAction {
    @Override
    public void execute() {
        System.out.println("Start play!");
    }
}
