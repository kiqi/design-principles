package top.kiqi.design.pattern.behavioral.momente.multi;

import java.util.Stack;

public class Caretaker {
    public IMemonte getMemonte() {
        return STACK.pop();
    }

    public void addMemonte(IMemonte memonte) {
        STACK.push(memonte);
    }

    private final Stack<IMemonte> STACK = new Stack<>();
}