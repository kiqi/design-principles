package top.kiqi.design.pattern.behavioral.momente.multi;

// 黑箱模式 - 栈式结构多备份备忘录
public class Test {
    public static void main(String[] args) {
        Originator originator = new Originator("Stop");  // 1. Stop

        // 创建备份
        Caretaker caretaker = new Caretaker();
        IMemonte memonte = originator.createMonte();            // IMemonte是空接口，IMemonte对客户端而言屏蔽了功能细节
        caretaker.addMemonte(memonte);

        // 更改状态
        originator.setState("Start");                           // 2. Start
        caretaker.addMemonte(originator.createMonte());

        originator.setState("Run");                             // 3. Run

        // 恢复备份
        originator.restoreMemonte(caretaker.getMemonte());      // 4. Start

        originator.restoreMemonte(caretaker.getMemonte());      // 5. Stop
    }
}
