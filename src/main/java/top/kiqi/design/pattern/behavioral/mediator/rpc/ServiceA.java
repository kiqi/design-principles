package top.kiqi.design.pattern.behavioral.mediator.rpc;

// 服务A 提供记录功能
public class ServiceA extends AbstractService{
    private Mediator mediator;

    public ServiceA(){
    }

    @Override
    public void register() {
        mediator = Mediator.getInstance();
        mediator.register("ServiceA",this);
    }

    @Override
    protected Object receive(String methodId, Object[] params) {
        if("record".equals(methodId)){
            return record(params);
        }
        return null;
    }

    private Object record(Object[] params){
        System.out.println("ServiceA - 执行ServiceA.record功能");
        return "OK!";
    }
}
