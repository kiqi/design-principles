package top.kiqi.design.pattern.behavioral.strategy;

public class Context{
    private IStrategy strategy = null;

    Context(IStrategy strategy){
        this.strategy = strategy;
    }

    public void algorithm(){
        strategy.algorithm();
    }
}
