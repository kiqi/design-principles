package top.kiqi.design.pattern.behavioral.strategy;

public class StrategyA implements IStrategy{
    @Override
    public void algorithm() {
        System.out.println("strategy A");
    }
}
