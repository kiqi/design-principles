package top.kiqi.design.pattern.behavioral.mediator.chatRoom;

import top.kiqi.design.tools.CommonUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ChatRoom {
    List<Message> messages = new ArrayList<>();

    public void receive(User user, String message){
        messages.add(new Message(user.getUsername(), new Date(), message));
    }

    public void show(){
        for(Message message : messages){
            System.out.println(message.username + "   " + CommonUtils.simpleFormatDate(message.date) + "\n" + message.message);
        }
    }

    private class Message {
        private String username;
        private Date date;
        private String message;

        public Message(String username, Date date, String message) {
            this.username = username;
            this.date = date;
            this.message = message;
        }
    }
}
