package top.kiqi.design.pattern.behavioral.momente.black_box;

public class Caretaker {
    public IMemonte getMemonte() {
        return memonte;
    }

    public void setMemonte(IMemonte memonte) {
        this.memonte = memonte;
    }

    private IMemonte memonte;
}