package top.kiqi.design.pattern.behavioral.chain;

public abstract class Handler {
    // 指向下一个节点的
    protected Handler next;

    public void setNext(Handler next){
        this.next = next;
    };

    // 主要的处理逻辑 - ①判断是否需要处理请求 ②判断是否要传递给下一个节点
    public abstract void handlerRequest(MemberInfo memberInfo);

    // 模板方法
    protected void nextHandlerIfExist(MemberInfo memberInfo){
        if(this.next != null){
            this. next.handlerRequest(memberInfo);
        }
    }

    // 链式构造器 - 建造者模式，简化链的构造过程
    public static class Builder{
        private Handler head;
        private Handler tail;

        public Builder addHandler(Handler handler){
            if(this.head == null){
                this.head = this.tail = handler;
            } else {
                this.tail.setNext(handler);
                this.tail = this.tail.next;
            }
            return this;
        }

        public Handler build(){
            return head;
        }
    }
}
