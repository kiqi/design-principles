package top.kiqi.design.pattern.behavioral.command;

public class Test {
    public static void main(String[] args) {
        Invoker controller = new Invoker();
        controller.actios(Invoker.Commands.PLAY);
        controller.actios(Invoker.Commands.TURN_UP);
        controller.actios(Invoker.Commands.TURN_DOWN);
        controller.actios(Invoker.Commands.STOP);
    }
}
