package top.kiqi.design.pattern.behavioral.delegate;

public class ExecutorB implements Executable{
    @Override
    public void execute(String command) {
        System.out.println("ExecutorB - Do task " + command);
    }
}
