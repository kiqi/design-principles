package top.kiqi.design.pattern.behavioral.momente.white_box;

public class Memonte {

    private String state;

    public Memonte(String state){
        this.state = state;
    }

    public String getState() {
        return this.state;
    }
}
