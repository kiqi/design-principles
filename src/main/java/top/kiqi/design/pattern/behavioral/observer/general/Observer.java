package top.kiqi.design.pattern.behavioral.observer.general;

public class Observer<T> implements IObserver<T>{
    @Override
    public void update(T event) {
        System.out.println("Observer：receive message : " + event);
    }
}
