package top.kiqi.design.pattern.behavioral.mediator.rpc;

public class Test {
    public static void main(String[] args) {
        Mediator mediator = Mediator.getInstance();
        new ServiceA().register();
        new ServiceB().register();

        mediator.relay("ServiceB","show",new Object[]{"String"});
    }
}
