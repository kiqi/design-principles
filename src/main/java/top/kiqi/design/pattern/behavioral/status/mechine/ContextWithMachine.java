package top.kiqi.design.pattern.behavioral.status.mechine;

// 上下文信息，用于维护所有状态对象以及当前状态信息，同时作为客户端的访问入口
public class ContextWithMachine implements IStatus {
    private AbstractStatus status = null;

    public ContextWithMachine() {
        this.status = StatusMechine.STOP_CLOSE_STATUS;
    }

    protected void setStatus(AbstractStatus status) {
        this.status = status;
    }

    @Override
    public void run() {
        this.status.run();
        this.status = StatusMechine.trans(this.status,StatusMechine.RUN_ACTION);
    }

    @Override
    public void stop() {
        this.status.stop();
        this.status = StatusMechine.trans(this.status,StatusMechine.STOP_ACTION);
    }

    @Override
    public void open() {
        this.status.open();
        this.status = StatusMechine.trans(this.status,StatusMechine.OPEN_ACTION);
    }

    @Override
    public void close() {
        this.status.close();
        this.status = StatusMechine.trans(this.status,StatusMechine.CLOSE_ACTION);
    }
}
