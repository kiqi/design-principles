package top.kiqi.design.pattern.behavioral.command.actions;

public class StopAction implements ICommandAction {
    @Override
    public void execute() {
        System.out.println("Stop play!");
    }
}
