package top.kiqi.design.pattern.behavioral.strategy;

public enum EnumStrategy {
    ADD(){
        @Override
        public int exec(int a, int b) {
            return a + b;
        }
    },
    SUB(){
        @Override
        public int exec(int a, int b) {
            return a - b;
        }
    };

    private EnumStrategy(){}

    public abstract int exec(int a, int b);
}
