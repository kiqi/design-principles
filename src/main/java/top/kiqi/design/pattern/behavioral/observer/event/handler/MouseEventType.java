package top.kiqi.design.pattern.behavioral.observer.event.handler;

public final class MouseEventType {
    public static final String ON_CLICK = "click";
    public static final String ON_MOVE = "move";
}
