package top.kiqi.design.pattern.behavioral.mediator.general;

public class Mediator {
    private ConcreteColleageA concreteColleageA = new ConcreteColleageA(this);
    private ConcreteColleageB concreteColleageB = new ConcreteColleageB(this);


    public void doMethodA() {
        System.out.print("Mediator call -> ");
        concreteColleageA.selfMethodA();
    }

    public void doMethodB() {
        System.out.print("Mediator call -> ");
        concreteColleageB.selfMethodB();
    }
}
