package top.kiqi.design.pattern.behavioral.template.jdbc;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Tom.
 */
public class MemberDao extends JdbcTemplate {
    public MemberDao(DataSource dataSource) {
        super(dataSource);
    }

    public List<Member> selectAll(){
        String sql = "select * from kiqi";

        RowMapper rowMapper = (rs,i) ->{
            try {
                Member member = new Member();
                member.setId(rs.getInt("id"));
                member.setUsername(rs.getString("username"));
                member.setPassword(rs.getString("password"));
                member.setAge(rs.getInt("age"));
                member.setAddr(rs.getString("addr"));
                return member;
            }catch (SQLException e){
                e.printStackTrace();
                return null;
            }
        };

        return super.executeQuery(sql, rowMapper,null);
    }

    public int insert(Member member){
        String sql = "insert into kiqi(username, password, age, addr) " +
                "VALUES('" + member.getUsername() + "', '" + member.getPassword()+ "', " + member.getAge() + " , '" + member.getAddr() + "')";

        return super.executeUpdate(sql);
    }

    public int update(Member member){
        StringBuilder sql = new StringBuilder("update kiqi set ");
                if(null != member.getUsername()){
                    sql.append("username = '").append(member.getUsername()).append("',");
                }

                if(null != member.getUsername()){
                    sql.append("password = '").append(member.getPassword()).append("',");
                }

                if(null != member.getUsername()){
                    sql.append("age = ").append(member.getAge()).append(",");
                }

                if(null != member.getUsername()){
                    sql.append("addr = '").append(member.getAddr()).append("',");
                }

                sql.append("WHERE username = '").append(member.getUsername()).append("'");

        return super.executeUpdate(sql.toString().replace(",WHERE"," WHERE"));
    }
}
