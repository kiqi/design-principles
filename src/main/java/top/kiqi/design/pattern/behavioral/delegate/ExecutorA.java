package top.kiqi.design.pattern.behavioral.delegate;

public class ExecutorA implements Executable{
    @Override
    public void execute(String command) {
        System.out.println("ExecutorA - Do task " + command);
    }
}
