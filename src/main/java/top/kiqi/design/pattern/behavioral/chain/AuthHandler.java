package top.kiqi.design.pattern.behavioral.chain;

public class AuthHandler extends Handler{
    @Override
    public void handlerRequest(MemberInfo memberInfo) {
        if(memberInfo.getAuth() == 1){
            System.out.println("验证通过");
            nextHandlerIfExist(memberInfo);
        }else {
            System.out.println("验证失败");
        }
    }
}
