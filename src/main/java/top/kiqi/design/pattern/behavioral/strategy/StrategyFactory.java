package top.kiqi.design.pattern.behavioral.strategy;

import java.util.HashMap;

public class StrategyFactory {
    private static HashMap<String, IStrategy> strategyContainer = null;

    static{
        strategyContainer = new HashMap<>();
        strategyContainer.put(StrategyType.A,new StrategyA());
        strategyContainer.put(StrategyType.B,new StrategyB());
    }

    // 现在的策略执行类存放在容器中，属于共享使用(单例)
    public IStrategy getStrategy(String stratrgyName){
        return strategyContainer.get(stratrgyName);
    }

    // 维护一份策略列表，用户根据策略常量创建对应的策略
    public static final class StrategyType{
        public static final String A = "A";
        public static final String B = "B";
    }
}
