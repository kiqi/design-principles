package top.kiqi.design.pattern.behavioral.strategy;

// 策略模式，定义一组算法，将每个算法都封装起来，并且使它们之间可以互换。
public class Test {
    public static void main(String[] args) {
        // 普通策略模式
        Context contextA = new Context(new StrategyA());
        contextA.algorithm();

        // 策略模式与工厂模式合并使用，工厂模式负责创建策略类(工厂模式也维护了一份策略常量类)
        StrategyFactory strategyFactory = new StrategyFactory();
        IStrategy strategyB = strategyFactory.getStrategy(StrategyFactory.StrategyType.B);
        Context contextB = new Context(strategyB);
        contextB.algorithm();

        // 枚举策略
        System.out.println(EnumStrategy.ADD.exec(1,2));
    }
}
