package top.kiqi.design.pattern.behavioral.template.jdbc;

/**
 * Created by Tom.
 */
public class Member {

    private int id;
    private String username;
    private String password;
    private int age;
    private String addr;

    public Member() {
    }

    public Member(String username, String password, int age, String addr) {
        this.username = username;
        this.password = password;
        this.age = age;
        this.addr = addr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", addr='" + addr + '\'' +
                '}';
    }
}
