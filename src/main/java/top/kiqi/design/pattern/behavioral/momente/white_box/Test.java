package top.kiqi.design.pattern.behavioral.momente.white_box;

// 白箱模式备忘录 - 用户端获取到备忘录对象后可以调用响应接口(甚至通过反射修改属性 - 破坏安全性)
public class Test {
    public static void main(String[] args) {
        Originator originator = new Originator("Stop");  // 1. Stop

        // 创建备份
        Caretaker caretaker = new Caretaker();
        Memonte memonte = originator.createMonte();             // 白箱模式下，memonte对客户端而言是功能透明的
        System.out.println(memonte.getState());
        caretaker.setMemonte(memonte);

        // 更改状态
        originator.setState("Start");                           // 2. Start

        // 恢复备份
        originator.restoreMemonte(caretaker.getMemonte());      // 3. Stop
    }
}
