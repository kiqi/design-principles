package top.kiqi.design.pattern.behavioral.status.mechine;

public class StopStatus extends AbstractStatus {
    @Override
    public String getStatusName() {
        return "STOP";
    }

    @Override
    public void run() {
        System.out.println("command run - start run!");
    }

    @Override
    public void stop() {
        System.out.println("command stop - keep stop_close!");
    }

    @Override
    public void open() {
        System.out.println("command open - open the door!");
    }

    @Override
    public void close() {
        System.out.println("command close - keep stop_close!");
    }
}
