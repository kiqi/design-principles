package top.kiqi.design.pattern.behavioral.status.mechine;

// 状态机的实现，将状态转换注册到状态机中，状态的转换由状态机负责。
public class Test {
    public static void main(String[] args) {
        ContextWithMachine context = new ContextWithMachine();  // 初始状态为 stop_close
        context.close();                  // 维持状态不变 stop_close
        context.open();                   // 跳转到open状态
        context.close();                  // 跳转到stop_close状态
        context.run();                    // 跳转到run状态
        context.close();                  // run状态下，无法执行close指令 - do nothing
        context.stop();                   // 跳转到stop_close状态
    }
}
