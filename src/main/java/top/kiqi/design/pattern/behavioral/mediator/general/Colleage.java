package top.kiqi.design.pattern.behavioral.mediator.general;

// 同事抽象，封装共性(当同事对象各有不同功能时，具体功能只能由子类自行实现，而无法作为共性封装)
public abstract class Colleage {
    // 同事类需要持有中介者的引用，通过中介者实现与其他同事类的交互
    protected Mediator mediator;

    protected Colleage(Mediator mediator){
        this.mediator = mediator;
    }
}
