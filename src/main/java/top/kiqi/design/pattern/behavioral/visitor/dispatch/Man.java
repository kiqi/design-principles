package top.kiqi.design.pattern.behavioral.visitor.dispatch;

public class Man implements IPerson {
    private static final String SEX = "man";

    @Override
    public String getSex() {
        return SEX;
    }
}
