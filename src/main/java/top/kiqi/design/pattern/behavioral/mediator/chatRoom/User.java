package top.kiqi.design.pattern.behavioral.mediator.chatRoom;

public class User {
    private String username;

    public User(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void chat(ChatRoom chatRoom, String message){
        chatRoom.receive(this,message);
    }
}
