package top.kiqi.design.pattern.behavioral.iterator;

import org.openjdk.jmh.annotations.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput) // 测试类型：吞吐量
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS) // 预热 5 轮，每次 1s
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS) // 测试 5 轮，每次 2s
//@Fork(1) // fork 1 个线程
@State(Scope.Thread) // 每个测试线程一个实例
public class MapTest {
    private static HashMap<String,Object> map = null;
    static {
        map = new HashMap<>(128);
        for(int i = 0;i < 10;i++){
            map.put(UUID.randomUUID().toString(),UUID.randomUUID());
        }
    }


    // 1. foreach entrySet遍历
    @Benchmark
    public static void foreachEntrySet(){
        // 1. EntrySet - foreach
        String key;
        Object value;
        for (Map.Entry<String, Object> entry : map.entrySet()){
            key = entry.getKey();
            value = entry.getValue();
        }
    }

    // 2. iterator entrySet遍历
    @Benchmark
    public static void iteratorEntrySet(){
        String key;
        Object value;
        Map.Entry<String,Object> entry;
        Iterator<Map.Entry<String,Object>> entrySetIterator = map.entrySet().iterator();
        while (entrySetIterator.hasNext()){
            entry = entrySetIterator.next();
            key = entry.getKey();
            value = entry.getValue();
        }
    }

    // 3. foreach keySet遍历
    @Benchmark
    public static void foreachKeySet(){
        // 1. EntrySet - foreach
        Object value;
        for (String key : map.keySet()){
            value = map.get(key);
        }
    }

    // 4. iterator keySet遍历
    @Benchmark
    public static void iteratorKeySet(){
        String key;
        Object value;
        Iterator<String> keySetIterator =  map.keySet().iterator();
        while (keySetIterator.hasNext()){
            key = keySetIterator.next();
            value = map.get(key);
        }
    }

    // 5. lambda遍历
    @Benchmark
    public static void lambda(){
        map.forEach((key,value) -> {
        });
    }

    // 6. stream串行遍历
    @Benchmark
    public static void stream(){
        map.entrySet().stream().forEach((entry) -> {
            entry.getKey();
            entry.getValue();
        });
    }

    // 7. stream并行遍历
    @Benchmark
    public static void parallelStream(){
        map.entrySet().parallelStream().forEach((entry) -> {
            entry.getKey();
            entry.getValue();
        });
    }
}
