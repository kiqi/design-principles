package top.kiqi.design.pattern.behavioral.interpreter.calculate.expression;

import top.kiqi.design.pattern.behavioral.interpreter.tools.OperatorUtil;
import java.util.Stack;

// 计算器(解释器)，通常需要对语句进行正确性判断，并进行词法解析和语法解析，最后得出执行结果。
public class Calculate {
    private Stack<SymbolExpression> symbolStack = new Stack<>();
    private Stack<VarExpression> varExpress = new Stack<>();

    private int baseLever = 0;

    public Calculate(String expression){
        this.parse(expression);
    }

    private void parse(String expression) {
        expression = expression.replaceAll(" ","");
        char[] express = expression.toCharArray();
        int size = express.length;

        StringBuilder temp = new StringBuilder();
        for(int i = 0;i < size;i++){
            // 当'('出现时，其之后的非终结符号等级提高，
            // 当')'出现时，其之后的非终结符号等级降低。从而实现(expression)的功能。
            if(express[i] == '('){
                baseLever = baseLever + 10;
            }else if(express[i] == ')'){
                baseLever = baseLever - 10;
            } else if(OperatorUtil.isOperator(express[i])){
                // 将之前的数值推入var,并清空var中缓存
                varExpress.push(new VarExpression(Integer.parseInt(temp.toString())));
                temp = new StringBuilder();

                // 获取到当前表达式符号
                SymbolExpression symbolExpress = OperatorUtil.getSymbolExpression(express[i],baseLever);

                // 标记A：当前表达式符号等级低于栈顶符号，则先计算栈顶符号
                while (!symbolStack.isEmpty() && symbolExpress.getLevel() <= (symbolStack.peek()).getLevel()){
                    SymbolExpression expressA = symbolStack.pop();
                    varExpress.push(new VarExpression(expressA.interpret(varExpress.pop(),varExpress.pop())));
                }
                symbolStack.push(symbolExpress);
            }else {
                // 如果是数字，则加入数字缓存中，知道碰到第一个非数字位时，转换为值存入var
                temp.append(express[i]);
            }
        }
        // 将最后一个数值存入var
        varExpress.push(new VarExpression(Integer.parseInt(temp.toString())));

        // 所有表达式均已解析完成，将栈中符号依次计算(此时，经过标记A的代码，symbol栈中符号等级由栈顶到栈尾递减)
        while (!symbolStack.isEmpty()){
            SymbolExpression expressA = symbolStack.pop();
            varExpress.push(new VarExpression(expressA.interpret(varExpress.pop(),varExpress.pop())));
        }
    }

    public int getValue() {
        return varExpress.pop().interpret();
    }
}
