package top.kiqi.design.pattern.behavioral.delegate.mvc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

public class DispatcherServlet extends HttpServlet {

    private static ConcurrentHashMap<String, Method> handlerMapping = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Object> ioc = new ConcurrentHashMap<>();

    {init();}

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doDispatch(req, resp);
    }

    private void doDispatch(HttpServletRequest req, HttpServletResponse resp){
        try {
            String url = req.getRequestURI();
            Method method = handlerMapping.get(url);
            method.invoke(ioc.get(url),req,resp);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void doDispatch(String uri){
        try {
            Method method = handlerMapping.get(uri);
            method.invoke(ioc.get(uri),null);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void init(){
        try {
            handlerMapping.putIfAbsent("/selectAll",MemberController.class.getMethod("selectAll"));
            ioc.putIfAbsent("/selectAll",new MemberController());
        } catch (NoSuchMethodException e){
            e.printStackTrace();
        }
    }
}
