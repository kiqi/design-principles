package top.kiqi.design.pattern.behavioral.chain;

public class Test {
    public static void main(String[] args) {
        MemberInfo memberInfo = new MemberInfo("kiqi007","i20132014");
        Handler handler = new Handler.Builder()
                            .addHandler(new LoginHandler())
                            .addHandler(new AuthHandler())
                            .build();
        handler.handlerRequest(memberInfo);
    }
}
