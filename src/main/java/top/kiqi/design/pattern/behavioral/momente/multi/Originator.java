package top.kiqi.design.pattern.behavioral.momente.multi;

public class Originator {
    private String state;

    public Originator(String state){
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public IMemonte createMonte(){
        return new Memonte(this.state);
    }

    public void restoreMemonte(IMemonte memonte){
        this.state = ((Memonte)memonte).getState();
    };

    public void show(){
        System.out.println("State = " + this.state);
    }

    // 黑相备忘录，Memonte响应的接口仅Originator类内部可用
    private class Memonte implements IMemonte {
        private String state;

        private Memonte(String state){
            this.state = state;
        }

        private String getState() {
            return this.state;
        }
    }
}
