package top.kiqi.design.pattern.behavioral.observer.community;

// 发布订阅模式 - 用户关注模型，
// Context中维护用户与用户之间的关注关系，当用户将消息提交给Context时，Context查找到对应的关注关系，并通知相关人员。
public class Test {
    public static void main(String[] args) {
        Context context = new Context();
        IUser userA = new IUser("张三",context);
        IUser lisi = new IUser("李四",context);
        IUser userC = new IUser("王五",context);
        IUser kiqi = new IUser("kiqi",context);

        context.subscribe("李四",userC);
        context.subscribe("kiqi",userA);
        context.subscribe("kiqi",lisi);

        kiqi.publish("今天在敲代码！");
        lisi.publish("kiqi说他今天没空，要敲代码！");
    }
}
