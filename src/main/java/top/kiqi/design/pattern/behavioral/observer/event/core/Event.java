package top.kiqi.design.pattern.behavioral.observer.event.core;

import java.lang.reflect.Method;

public class Event {
    // 发布者
    private Object sourece;
    // 订阅人
    private EventListener target;
    // 事件名
    private String eventType;
    // 事件回调
    private Method callback;
    // 触发事件
    private long time;

    public Event(EventListener target, String eventType, Method callback) {
        this.target = target;
        this.eventType = eventType;
        this.callback = callback;
    }

    public Object getSourece() {
        return sourece;
    }

    public void setSourece(Object sourece) {
        this.sourece = sourece;
    }

    public EventListener getTarget() {
        return target;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Method getCallback() {
        return callback;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Event{" +
                "sourece=" + sourece +
                ", target=" + target +
                ", eventType='" + eventType + '\'' +
                ", callback=" + callback +
                ", time=" + time +
                '}';
    }
}
