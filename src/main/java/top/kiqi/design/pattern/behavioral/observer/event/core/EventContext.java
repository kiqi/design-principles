package top.kiqi.design.pattern.behavioral.observer.event.core;

import top.kiqi.design.tools.CommonUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EventContext {
    // 存储事件相关信息 - Event中包含订阅人及其回调方法，只需通过eventType即可完成通知
    protected Map<String, Event> events = new ConcurrentHashMap<>();

    // 注册 事件名 与 订阅人之间的关系
    public void addListener(String eventType, EventListener target, Method callback){
        events.putIfAbsent(eventType, new Event(target, eventType, callback));
    }

    public void addListener(String eventType, EventListener target){
        try {
            events.putIfAbsent(eventType, new Event(target, eventType, target.getClass().getMethod("on" + CommonUtils.toUpperFirstCase(eventType), Event.class)));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // 触发事件
    private void trigger(Event event){
        event.setSourece(this);
        event.setTime(System.currentTimeMillis());

        if(event.getCallback() != null){
            try {
                // 获取到回调方法之后，通过invoke(object, params...)方法反射回调
                event.getCallback().invoke(event.getTarget(), event);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    protected void trigger(String trigger){
        if(this.events.containsKey(trigger)) {
            trigger(events.get(trigger));
        }
    }
}
