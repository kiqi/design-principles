package top.kiqi.design.pattern.behavioral.status.general;

// 上下文信息，用于维护所有状态对象以及当前状态信息，同时作为客户端的访问入口
public class Context implements IStatus{
    protected final IStatus OPEN = new OpenStatus(this);
    protected final IStatus STOP_CLOSE = new StopStatus(this);
    protected final IStatus RUN = new RunStatus(this);

    private IStatus status = null;

    public Context() {
        this.status = STOP_CLOSE;
    }

    protected void setStatus(IStatus status) {
        this.status = status;
    }

    @Override
    public void run() {
        this.status.run();
    }

    @Override
    public void stop() {
        this.status.stop();
    }

    @Override
    public void open() {
        this.status.open();
    }

    @Override
    public void close() {
        this.status.close();
    }
}
