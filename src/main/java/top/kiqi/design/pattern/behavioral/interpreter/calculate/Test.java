package top.kiqi.design.pattern.behavioral.interpreter.calculate;

import org.springframework.expression.spel.standard.SpelExpressionParser;
import top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.Calculate;

// 解释器模式，通过定义词法规则和语法规则，并提供一个对应的解释器，允许用户端自行构造语句执行不同逻辑
public class Test {
    public static void main(String[] args) {
        String express = "1+((1+2)*(3-1)+3)/2 + 8";

        // 自写计算表达式
        System.out.println(new Calculate(express).getValue());

        // Spring封装的表达式
        System.out.println(new SpelExpressionParser().parseExpression(express).getValue());;
    }
}
