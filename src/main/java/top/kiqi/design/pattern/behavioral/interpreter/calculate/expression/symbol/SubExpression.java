package top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.symbol;

import top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.IExpression;
import top.kiqi.design.pattern.behavioral.interpreter.calculate.expression.SymbolExpression;

public class SubExpression extends SymbolExpression {
    public SubExpression(int baseLevel) {
        super(baseLevel);
    }

    @Override
    public int interpret(IExpression... params) {
        return params[1].interpret() - params[0].interpret();
    }
}
