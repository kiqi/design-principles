package top.kiqi.design.pattern.behavioral.strategy;

public class StrategyB implements IStrategy{
    @Override
    public void algorithm() {
        System.out.println("strategy B");
    }
}
