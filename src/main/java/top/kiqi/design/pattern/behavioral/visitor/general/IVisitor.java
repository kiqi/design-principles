package top.kiqi.design.pattern.behavioral.visitor.general;

public interface IVisitor {
    void visit(ElementA element);

    void visit(ElementB  element);
}
