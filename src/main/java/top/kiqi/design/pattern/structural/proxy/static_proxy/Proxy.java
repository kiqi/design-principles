package top.kiqi.design.pattern.structural.proxy.static_proxy;

public class Proxy implements ISubject{
    private ISubject subject = null;

    public Proxy(ISubject subject){
        this.subject = subject;
    }

    @Override
    public void work() {
        long startTime = System.currentTimeMillis();
        this.subject.work();
        System.out.println("接口耗时：" + (System.currentTimeMillis() - startTime));
    }
}
