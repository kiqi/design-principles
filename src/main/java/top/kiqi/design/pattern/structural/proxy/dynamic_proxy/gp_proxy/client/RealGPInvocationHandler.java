package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.gp_proxy.client;

import top.kiqi.design.pattern.structural.proxy.dynamic_proxy.gp_proxy.proxy.GPClassLoader;
import top.kiqi.design.pattern.structural.proxy.dynamic_proxy.gp_proxy.proxy.GPInvocationHandler;
import top.kiqi.design.pattern.structural.proxy.dynamic_proxy.gp_proxy.proxy.GPProxy;

import java.lang.reflect.Method;

public class RealGPInvocationHandler implements GPInvocationHandler {

    private ISubject subject = null;

    private RealGPInvocationHandler(ISubject subject){
        this.subject = subject;
    }

    public static ISubject getInstance(ISubject subject){
        GPInvocationHandler gpInvocationHandler = new RealGPInvocationHandler(subject);
        Class<?> clazz = subject.getClass();
        return (ISubject) GPProxy.newProxyInstance(new GPClassLoader(),clazz.getInterfaces(),gpInvocationHandler);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object result = method.invoke(subject,args);
        after();
        return result;
    }

    private void before() {
        System.out.println("before!");
    }

    private void after() {
        System.out.println("after!");
    }
}
