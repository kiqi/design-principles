package top.kiqi.design.pattern.structural.adapter.class_adapter;

// 类适配器
// 适配器与源目标：通过继承实现信息通信
// 适配器需实现目标对象接口
public class Test {
    public static void main(String[] args) {
        Target target = new Adapter();
        System.out.println(target.request());


    }
}
