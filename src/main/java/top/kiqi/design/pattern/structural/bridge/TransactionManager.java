package top.kiqi.design.pattern.structural.bridge;

public class TransactionManager extends DriverManager{

    public TransactionManager(IDriver driver) {
        super(driver);
    }

    @Override
    public boolean execute(String conn, String sql) {
        System.out.println("begin!");
        boolean result = super.execute(conn, sql);
        System.out.println("commit!");
        return result;
    }
}
