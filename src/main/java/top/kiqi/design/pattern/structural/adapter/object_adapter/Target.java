package top.kiqi.design.pattern.structural.adapter.object_adapter;

public interface Target {
    int request();
}
