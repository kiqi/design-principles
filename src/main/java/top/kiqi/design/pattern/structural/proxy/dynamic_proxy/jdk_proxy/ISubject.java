package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.jdk_proxy;

public interface ISubject {
    void work();
}
