package top.kiqi.design.pattern.structural.adapter.object_adapter;

public class Adapter implements Target {

    private Adaptee adaptee = null;

    public Adapter(Adaptee adaptee){
        this.adaptee = adaptee;
    }

    @Override
    public int request() {
        return adaptee.specificRequest() / 2;
    }
}
