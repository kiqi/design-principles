package top.kiqi.design.pattern.structural.composite.safe;

// 抽象父类，维护叶和枝之间的共性(此时，共性多为枝作为个体部分的操作)
public abstract class Directory {
    protected String name;

    protected Directory(String name) {
        this.name = name;
    }

    public abstract void show(String prefix);
}
