package top.kiqi.design.pattern.structural.decorate;

public class LineReadDecorator extends AbstractDecorator{
    public LineReadDecorator(AbstractComponent component) {
        super(component);
    }

    @Override
    void read() {
        super.read();
        System.out.print("by line!");
    }
}
