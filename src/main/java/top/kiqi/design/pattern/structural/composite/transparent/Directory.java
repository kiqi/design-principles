package top.kiqi.design.pattern.structural.composite.transparent;

import java.util.List;

// 抽象父类，维护叶和枝之间的共性，以及枝的组合相关操作
public abstract class Directory {
    protected String name;

    protected Directory(String name) {
        this.name = name;
    }

    public abstract void show(String prefix);

    public abstract boolean isFolder();

    // 枝的组合相关操作
    public abstract void addDirectory(Directory directory);

    public abstract void removeDirectory(Directory directory);

    public abstract List<Directory> getDirectorys();

    public abstract void listDirectorys(String prefix);
}
