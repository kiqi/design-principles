package top.kiqi.design.pattern.structural.bridge;

// 主枝 - driver职责相关的代码通过依赖的方式引入。
public class DriverManager {
    private IDriver driver = null;

    public DriverManager(IDriver driver){
        this.driver = driver;
    }

    public String getConnection(){
        // 前置逻辑
        String conn = this.driver.getConnection();
        // 后置逻辑
        return conn;
    }

    public boolean execute(String conn, String sql){
        // 前置逻辑
        boolean result = driver.executeUpdate(conn,sql);
        // 后置逻辑
        return result;
    }
}
