package top.kiqi.design.pattern.structural.flyweight.general;

public interface IFlyweight {
    void operation();
    void setExtrinsicState(String extrinsicState);
}
