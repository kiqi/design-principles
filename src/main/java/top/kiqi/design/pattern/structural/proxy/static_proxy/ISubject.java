package top.kiqi.design.pattern.structural.proxy.static_proxy;

public interface ISubject {
    void work();
}
