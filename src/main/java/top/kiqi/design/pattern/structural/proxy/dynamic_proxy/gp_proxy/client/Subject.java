package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.gp_proxy.client;

public class Subject implements ISubject {
    @Override
    public void work(Integer a) {
        System.out.println("Doing work!" + a);
    }
}
