package top.kiqi.design.pattern.structural.decorate;

public class BufferedReadDecorator extends AbstractDecorator{
    public BufferedReadDecorator(AbstractComponent component) {
        super(component);
    }

    @Override
    void read() {
        super.read();
        System.out.print("with buffered ");
    }
}
