package top.kiqi.design.pattern.structural.decorate;

public class Component extends AbstractComponent{
    @Override
    void read() {
        System.out.print("read bytes ");
    }
}
