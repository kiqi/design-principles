package top.kiqi.design.pattern.structural.adapter.class_adapter;

public interface Target {
    int request();
}
