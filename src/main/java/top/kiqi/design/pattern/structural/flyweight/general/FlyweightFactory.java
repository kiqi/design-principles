package top.kiqi.design.pattern.structural.flyweight.general;

import java.util.HashMap;
import java.util.Map;

public class FlyweightFactory {
    private static Map<String, IFlyweight> pool = new HashMap<>();

    public static IFlyweight getFlyweight(String intrinsicState){
        if(pool.containsKey(intrinsicState)){
            return pool.get(intrinsicState);
        }else {
            IFlyweight result = new ConcreteFlyweight(intrinsicState);
            pool.put(intrinsicState, result);
            return result;
        }
    }
}
