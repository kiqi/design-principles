package top.kiqi.design.pattern.structural.proxy.static_proxy;

public class Subject implements ISubject{
    @Override
    public void work() {
        System.out.println("Doing work!");
    }
}
