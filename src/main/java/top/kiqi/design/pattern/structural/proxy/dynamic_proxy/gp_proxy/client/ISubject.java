package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.gp_proxy.client;

public interface ISubject {
    void work(Integer a);
}
