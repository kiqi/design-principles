package top.kiqi.design.pattern.structural.facade;

public class Facade {
    private SystemA systemA = new SystemA();
    private SystemB systemB = new SystemB();
    private SystemC systemC = new SystemC();

    public void doA(){
        systemA.doA();
    }
    public void doB(){
        systemB.doB();
    }
    public void doC(){
        systemC.doC();
    }

    public void doWork(){
        doA();
        if(Math.random() > 0.5) {
            doB();
        }
        doC();
    }
}
