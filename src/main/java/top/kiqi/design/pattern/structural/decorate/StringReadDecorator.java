package top.kiqi.design.pattern.structural.decorate;

public class StringReadDecorator extends AbstractDecorator{
    public StringReadDecorator(AbstractComponent component) {
        super(component);
    }

    @Override
    void read() {
        super.read();
        System.out.print("by String!");
    }
}
