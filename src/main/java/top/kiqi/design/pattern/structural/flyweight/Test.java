package top.kiqi.design.pattern.structural.flyweight;

import top.kiqi.design.pattern.structural.flyweight.general.FlyweightFactory;
import top.kiqi.design.pattern.structural.flyweight.general.IFlyweight;

public class Test {
    public static void main(String[] args) {
         IFlyweight flyweight1 = FlyweightFactory.getFlyweight("haha");
         flyweight1.setExtrinsicState("flyweight1");
         flyweight1.operation();

        IFlyweight flyweight2 = FlyweightFactory.getFlyweight("baba");
        flyweight2.setExtrinsicState("flyweight2");
        flyweight2.operation();
        intergeTest();
    }

    // String常量池优化：在编译过程中出现的字符串引用均指向常量池，在运行阶段创建的字符串则重写在堆中创建一个字符串对象
    public static void stringTest() {
        String s1 = "hello";
        // s1 == s2 true - 均从String常量池中获取到常量引用
        String s2 = "hello";

        // s1 == s3 true - 编译时优化，常量之间将发生合并
        String s3 = "he" + "llo";

        // s1 == s4 false - 在运行时才能触发new指令创建对象，
        String s4 = "hel" + new String("lo");

        // s1 == s5 false - 在运行时会触发new指令创建对象，此时，常量池中存在一个String对象，在堆中又创建一个新的String对象
        // s4 == s5 false - 运行时创建的两个对象，引用不相同。
        String s5 = new String("hello");

        // s1 == s6 true - intern：从常量池中获取对应字符串引用
        String s6 = s5.intern();

        String s7 = "h";
        String s8 = "ello";

        // s1 == s9 false - 只要字符串链接过程中存在变量，则必定是运行时创建
        String s9 = s7 + s8;
        System.out.println("s1 == s2: " + (s1 == s2));//true
        System.out.println("s1 == s3: " + (s1 == s3));//true
        System.out.println("s1 == s4: " + (s1 == s4));//false
        System.out.println("s1 == s5: " + (s1 == s5));//false
        System.out.println("s4 == s5: " + (s4 == s5));//false
        System.out.println("s1 == s6: " + (s1 == s6));//true
        System.out.println("s1 == s9: " + (s1 == s9));//false
    }

    public static void intergeTest(){
        // a == b true
        Integer a = Integer.valueOf(100);
        Integer b = 100;

        // c == d false
        Integer c = Integer.valueOf(1000);
        Integer d = 1000;

        System.out.println("a==b:" + (a==b));
        System.out.println("c==d:" + (c==d));
    }
}
