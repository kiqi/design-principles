package top.kiqi.design.pattern.structural.adapter.object_adapter;

// 对象适配器
// 适配器与源目标：通过关联委托实现通信
// 适配器需实现目标对象接口
public class Test {
    public static void main(String[] args) {
        Target target = new Adapter(new Adaptee());
        System.out.println(target.request());
    }
}
