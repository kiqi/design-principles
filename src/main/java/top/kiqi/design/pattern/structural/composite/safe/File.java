package top.kiqi.design.pattern.structural.composite.safe;

// 叶节点
public class File extends Directory{
    public File(String name) {
        super(name);
    }

    @Override
    public void show(String prefix) {
        System.out.println(prefix + "File : " + name);
    }
}
