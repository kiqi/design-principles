package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.jdk_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class RealInvocationHandler implements InvocationHandler {
    private Object subject = null;

    // 方案1 - 单一职责
    public RealInvocationHandler(Object subject){
        this.subject = subject;
    }

    // 方案2 - 将创建代理对象的职责与invoke的职责合并，高层只关心获取到的proxy对象。
    public RealInvocationHandler(){};

    public Object getProxy(Object subject){
        this.subject = subject;
        Class<?> clazz = subject.getClass();
        return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object result = method.invoke(this.subject,args);
        after();
        return result;
    }

    public void before(){
        System.out.println("before!");
    }

    public void after(){
        System.out.println("after!");
    }
}
