package top.kiqi.design.pattern.structural.bridge;

// 抽象接口，作为Controller的依赖。
public interface IDriver {
    String getConnection();

    boolean executeUpdate(String conn, String sql);
}