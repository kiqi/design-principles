package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.gp_proxy.client;

public class Test {
    public static void main(String[] args) {
        ISubject subject = new Subject();
        ISubject proxy = RealGPInvocationHandler.getInstance(subject);
        proxy.work(5);
    }
}
