package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.cglib_proxy;

public class Subject{
    public void work() {
        System.out.println("Doing work!");
    }
}
