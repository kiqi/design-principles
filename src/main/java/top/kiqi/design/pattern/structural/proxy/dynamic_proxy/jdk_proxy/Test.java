package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.jdk_proxy;

import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.lang.reflect.Proxy;

// java proxy - 通过实现相同接口实现代理。
public class Test {
    public static void main(String[] args) {
        // 主题类
        ISubject subject = new Subject();

        // 功能增强类(实现功能增强的相关代码)
        RealInvocationHandler handler = new RealInvocationHandler(subject);

        // 代理类(实现了主题类的接口，对实现的每一个方法，获取方法相关的信息和参数，传递给handler.invoke()方法
        ISubject proxy = (ISubject)Proxy.newProxyInstance(subject.getClass().getClassLoader(),subject.getClass().getInterfaces(),handler);

        // 1. 在work方法中获取到方法信息(Method对象和params，传递给hanlder
        // 2. handler 使用反射 Method.invoke(target,params);实现方法调用
        proxy.work();

        // 将生成的代理对象写磁盘
        try {
            byte[] bytes = ProxyGenerator.generateProxyClass("$Proxy0", new Class[]{ISubject.class});
            FileOutputStream fos = new FileOutputStream("$Proxy0.class");
            fos.write(bytes);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
