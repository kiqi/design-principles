package top.kiqi.design.pattern.structural.adapter.class_adapter;

public class Adapter extends Adaptee implements Target{
    @Override
    public int request() {
        return super.specificRequest() / 2;
    }
}
