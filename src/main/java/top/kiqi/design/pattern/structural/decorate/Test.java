package top.kiqi.design.pattern.structural.decorate;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;

// 装饰器模式
public class Test {
    public static void main(String[] args) {
        AbstractComponent component = new Component();
        LineReadDecorator lineReadDecorator = new LineReadDecorator(component);
        lineReadDecorator.read();

        System.out.println();

        BufferedReadDecorator bufferedReadDecorator = new BufferedReadDecorator(component);
        StringReadDecorator stringReadDecorator = new StringReadDecorator(bufferedReadDecorator);
        stringReadDecorator.read();

        try {
            InputStream is = new FileInputStream(new File("HungryStaticSingleton.obj"));
            ObjectInputStream ois = new ObjectInputStream(is);
            ois.read();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
