package top.kiqi.design.pattern.structural.flyweight.general;

public class ConcreteFlyweight implements IFlyweight{
    // 内部状态，对象创建之后即不再改变，特性：共享
    private String intrinsicState;

    // 外部状态，在运行过程中可以被改变的状态(与请求相关的变量)，功能：复用
    private String extrinsicState;

    // 内部状态由构造方法进行设置，只在创建时设置
    public ConcreteFlyweight(String intrinsicState) {
        this.intrinsicState = intrinsicState;
    }

    // 外部状态通过方法传参，在生命周期内可以被多次修改(一般每次被获取时都会修改外部状态)
    public void setExtrinsicState(String extrinsicState) {
        this.extrinsicState = extrinsicState;
    }

    @Override
    public void operation() {
        System.out.println("Object address：" + System.identityHashCode(this));
        System.out.println("intrinsic State：" + this.intrinsicState);
        System.out.println("extrinsic State：" + this.extrinsicState);
    }
}
