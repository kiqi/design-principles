package top.kiqi.design.pattern.structural.proxy.dynamic_proxy.jdk_proxy;

public class Subject implements ISubject{
    @Override
    public void work() {
        System.out.println("Doing work!");
    }
}
