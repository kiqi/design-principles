package top.kiqi.design.pattern.structural.decorate;

public abstract class AbstractComponent {
    abstract void read();
}
