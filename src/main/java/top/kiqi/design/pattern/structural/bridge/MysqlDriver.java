package top.kiqi.design.pattern.structural.bridge;

public class MysqlDriver implements IDriver{
    @Override
    public String getConnection() {
        return "mysql conn";
    }

    @Override
    public boolean executeUpdate(String conn, String sql) {
        System.out.println(conn + " execute update");
        return true;
    }
}
