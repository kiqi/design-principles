package top.kiqi.design.pattern.structural.decorate;

public abstract class AbstractDecorator extends AbstractComponent{

    private AbstractComponent component = null;

    protected AbstractDecorator(AbstractComponent component){
        this.component = component;
    }

    @Override
    void read() {
        component.read();
    }
}
