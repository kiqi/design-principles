package top.kiqi.design.pattern.structural.proxy.static_proxy;

// 静态代理模式，代理类在主题类前后添加代码段，实现功能增强。
// 代理类与ISubject接口交互，无需关心主题类的具体实现。(解耦)
public class Test {
    public static void main(String[] args) {
        ISubject subject = new Subject();
        Proxy proxy = new Proxy(subject);

        proxy.work();
    }
}
