package top.kiqi.design.pattern.structural.composite.transparent;

import java.util.List;

// 叶节点，对于组合相关的操作，需做一定的拒绝处理
public class File extends Directory {
    public File(String name) {
        super(name);
    }

    @Override
    public void show(String prefix) {
        System.out.println(prefix + "File : " + name);
    }

    @Override
    public boolean isFolder() {
        return false;
    }

    @Override
    public void addDirectory(Directory directory) {
        System.out.println("Not folder, do nothing!");
    }

    @Override
    public void removeDirectory(Directory directory) {
        System.out.println("Not folder, do nothing!");
    }

    @Override
    public List<Directory> getDirectorys() {
        return null;
    }

    @Override
    public void listDirectorys(String prefix) {
        System.out.println("Not folder, do nothing!");
    }
}
