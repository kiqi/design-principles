package top.kiqi.design.pattern.structural.composite.safe;

// 安全的组合模式 - 抽象父类只维护共有部分
public class Test {
    public static void main(String[] args) {
        Folder folder = new Folder("零：");

            Folder folderA = new Folder("A：");
            folderA.addDirectory(new File("fileAA"));

            Folder folderB = new Folder("B：");
            folderB.addDirectory(new File("fileBA"));

        folder.addDirectory(folderA);
        folder.addDirectory(folderB);
        folder.addDirectory(new File("file零A"));
        folder.addDirectory(new File("file零B"));


        folder.listDirectorys("");
    }
}
