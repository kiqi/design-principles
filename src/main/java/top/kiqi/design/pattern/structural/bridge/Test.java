package top.kiqi.design.pattern.structural.bridge;

public class Test {
    public static void main(String[] args) {
        IDriver driver = new MysqlDriver();
        DriverManager driverManager = new DriverManager(driver);
        String conn = driverManager.getConnection();
        driverManager.execute(conn, "update kiqi set name = 'kiqi' where id = 6");
    }
}
