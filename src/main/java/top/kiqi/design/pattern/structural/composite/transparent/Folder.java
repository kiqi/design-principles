package top.kiqi.design.pattern.structural.composite.transparent;

import java.util.ArrayList;
import java.util.List;

// 枝节点，实现抽象父类方法之外，还要实现作为组合功能的操作
public class Folder extends Directory {
    List<Directory> directorys = new ArrayList<>();

    public Folder(String name) {
        super(name);
    }

    @Override
    public void show(String prefix) {
        System.out.println(prefix + "Folder : " + name);
    }

    @Override
    public boolean isFolder() {
        return true;
    }

    public void addDirectory(Directory directory){
        directorys.add(directory);
    }

    public void removeDirectory(Directory directory){
        directorys.remove(directory);
    }

    public List<Directory> getDirectorys(){
        return directorys;
    }

    public void listDirectorys(String prefix){
        for(Directory directory : directorys){
            if(directory instanceof Folder) {
                directory.show(prefix);
                ((Folder) directory).listDirectorys(prefix + "    ");
            }else {
                directory.show(prefix);
            }
        }
    }
}
