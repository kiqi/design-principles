package top.kiqi.design.pattern.creational.singleton.threadlocal;

// 线程级单例 - 使用ThreadLocal为每个线程私有维护一份实例对象
public class ThreadLocalSingleton {
    private static ThreadLocal<ThreadLocalSingleton> threadLocalSingleton = ThreadLocal.withInitial(ThreadLocalSingleton::new);

    private ThreadLocalSingleton(){}

    public static ThreadLocalSingleton getInstance(){
        return threadLocalSingleton.get();
    }
}
