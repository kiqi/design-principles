package top.kiqi.design.pattern.creational.builder.choose;

// 零件装配，构建者对象
public class StudyMethodBuilder {
    private StudyMethod studyMethod = new StudyMethod();

    public StudyMethodBuilder setStudyMethodTitle(String title){
        studyMethod.setMethodTitle(title);
        return this;
    }

    public StudyMethodBuilder addWatchVedeos(){
        studyMethod.setWatchVideos(true);
        return this;
    }

    public StudyMethodBuilder addDoHomework(){
        studyMethod.setDoHomework(true);
        return this;
    }

    public StudyMethodBuilder addTakeNotes(){
        studyMethod.setTakeNotes(true);
        return this;
    }

    public StudyMethod builder(){
        return studyMethod;
    }
}
