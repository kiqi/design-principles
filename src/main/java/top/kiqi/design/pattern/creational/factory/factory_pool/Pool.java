package top.kiqi.design.pattern.creational.factory.factory_pool;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Properties;

public abstract class Pool {
    // 配置文件
    protected String propertiesName = "connection.properties";

    // 最大连接数
    protected int maxConnect = 100;

    // 常驻连接数
    protected int normalConnect = 10;

    // 驱动全类名
    protected String driverName = null;
    
    // 驱动类
    private Driver driver;

    protected Pool(){
        init();
        loadDrive(driverName);
    }

    // 初始化配置文件
    protected void init(){
        try {
            InputStream is = Pool.class.getClassLoader().getResourceAsStream(propertiesName);
            Properties properties = new Properties();
            properties.load(is);
            this.driverName = properties.getProperty("driverName");
            this.maxConnect = Integer.parseInt(properties.getProperty("maxConnect"));
            this.normalConnect = Integer.parseInt(properties.getProperty("normalConnect"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // 将驱动装载到java DriverManager中
    protected void loadDrive(String driveName){
        try {
            driver = (Driver) Class.forName(driveName).newInstance();
            DriverManager.registerDriver(driver);
            System.out.println("成功注册JDBC驱动程序" + driveName);
        }catch (Exception e){
            System.out.println("无法注册JDBC驱动程序:" + driveName + ",错误:" + e);
        }
    }

    // 从连接池获取线程
    public abstract Connection getConnection();

    // 将线程释放回连接池
    public abstract void freeConnection(Connection conn);

    // 获取空闲线程数
    public abstract int getNumForFree();

    // 获取活跃的线程数
    public abstract int getNumForActive();

    // 注销连接池
    protected synchronized void release(){
        try {
            DriverManager.deregisterDriver(driver);
            System.out.println("撤销JDBC驱动程序 " + driver.getClass().getName());
        }catch (Exception e){
            System.out.println("无法撤销JDBC驱动程序的注册:" + driver.getClass().getName());
        }
    }
}
