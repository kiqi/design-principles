package top.kiqi.design.pattern.creational.factory.factory_pool;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

public class DBConnectionPool extends Pool{
    // 存放空闲线程的容器
    private Vector<Connection> freeConnections = new Vector<>();

    // 连接字符串
    private String url = null;

    // 权限用户
    private String username = null;

    // 权限密码
    private String password = null;

    private int checkedOut; //正在使用的连接数

    private static int freeNum = 0;// 空闲连接数

    private static int activeNum = 0;// 当前可用的连接数

    // 生成数据库连接池实例
    public static synchronized DBConnectionPool getInstance() {
        if(pool == null) {
            pool = new DBConnectionPool();
        }
        return pool;
    }
    private static DBConnectionPool pool = null;// 连接池实例变量

    // 创建连接池，并初始化链接
    private DBConnectionPool() {
        init();
        try {
            for (int i = 0; i < normalConnect; i++) { // 初始normalConn个连接
                Connection c = newConnection();
                if (c != null) {
                    freeConnections.addElement(c); //往容器中添加一个连接对象
                    freeNum++; //记录总连接数
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化
     * @throws IOException
     *
     */
    protected void init() {
        super.init();
        try {
            InputStream is = DBConnectionPool.class.getClassLoader().getResourceAsStream(propertiesName);
            Properties p = new Properties();
            p.load(is);
            this.username = p.getProperty("username");
            this.password = p.getProperty("password");
            this.url = p.getProperty("url");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Connection newConnection() {
        Connection conn = null;
        try {
            if (username == null) { // 用户,密码都为空
                conn = DriverManager.getConnection(url);
            } else {
                conn = DriverManager.getConnection(url, username, password);
            }
            System.out.println("连接池创建一个新的连接");
        } catch (SQLException e) {
            System.out.println("无法创建这个URL的连接" + url);
            e.printStackTrace();
            return null;
        }
        return conn;
    }

    @Override
    public synchronized Connection getConnection() {
        Connection con = null;
        if (freeConnections.size() > 0) { // 还有空闲的连接
            freeNum--;
            con = freeConnections.firstElement();
            freeConnections.removeElementAt(0);
            try {
                if (con.isClosed()) {
                    con = getConnection();
                }
            } catch (SQLException e) {
                con = getConnection();
            }
        } else if (maxConnect == 0 || checkedOut < maxConnect) { // 没有空闲连接且当前连接小于最大允许值,最大值为0则不限制
            con = newConnection();
        }
        if (con != null) { // 当前连接数加1
            checkedOut++;
        }
        return con;
    }

    @Override
    public synchronized void freeConnection(Connection conn) {
        freeConnections.addElement(conn);
        freeNum++;
        checkedOut--;
        notifyAll(); //解锁
    }

    @Override
    public int getNumForFree() {
        return freeNum;
    }

    @Override
    public int getNumForActive() {
        return checkedOut;
    }

    /**
     * 关闭所有连接
     */
    public synchronized void release() {
        try {
            //将当前连接赋值到 枚举中
            Enumeration allConnections = freeConnections.elements();
            //使用循环关闭所用连接
            while (allConnections.hasMoreElements()) {
                //如果此枚举对象至少还有一个可提供的元素，则返回此枚举的下一个元素
                Connection con = (Connection) allConnections.nextElement();
                try {
                    con.close();
                    freeNum--;
                } catch (SQLException e) {
                    System.out.println("无法关闭连接池中的连接");
                }
            }
            freeConnections.removeAllElements();
        } finally {
            super.release();
        }
    }
}
