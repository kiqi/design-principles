package top.kiqi.design.pattern.creational.factory.abstract_factory.produce;

public class WhiteMan extends AbstractMan{
    @Override
    public void talk() {
        System.out.println("I am a white human!");
    }

    @Override
    public void showColor() {
        System.out.println("white!");
    }
}
