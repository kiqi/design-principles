package top.kiqi.design.pattern.creational.factory.abstract_factory.factory;

import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.AbstractMan;
import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.BlackMan;
import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.WhiteMan;
import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.YellowMan;

public class ManFactory implements AbstractHumanFactory{
    @Override
    public AbstractMan createWhiteHuman() {
        return new WhiteMan();
    }

    @Override
    public AbstractMan createYellowHuman() {
        return new YellowMan();
    }

    @Override
    public AbstractMan createBlackHuman() {
        return new BlackMan();
    }

    public AbstractMan createMan() {
        return new WhiteMan();
    }
}
