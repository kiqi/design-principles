package top.kiqi.design.pattern.creational.prototype;

import java.util.ArrayList;

// 原型模式 - 浅克隆，实现cloneable接口，重写clone方法并放开访问权限
public class Test {
    public static void main(String[] args) throws CloneNotSupportedException{
        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("bb");
        list.add("ccc");

//        cloneTest(new PrototypeMember("kiqi", list));
//        deepCloneTest(new PrototypeMember("kiqi", list));
        serializableCloneTest(new PrototypeMember("kiqi", list));
//        reflectCloneTest(new PrototypeMember("kiqi", list));
//        jsonCloneTest(new PrototypeMember("kiqi", list));
    }

    public static void cloneTest(PrototypeMember p1){
        System.out.println("浅克隆！");
        // 复制后，两个PrototypeMember已不同
        PrototypeMember p2 = p1.clone();
        System.out.println(p1 == p2);
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());

        // 但两个PrototypeMember内的引用属性依然指向同一个对象 - 浅克隆
        p2.list.add("dddd");
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());
    }

    public static void deepCloneTest(PrototypeMember p1){
        System.out.println("递归clone - 深克隆！");

        PrototypeMember p2 = p1.deepcloneByCloneable();
        System.out.println(p1 == p2);
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());

        p2.list.add("dddd");
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());
    }

    public static void serializableCloneTest(PrototypeMember p1){
        System.out.println("序列化clone - 深克隆！");

        PrototypeMember p2 = p1.deepCloneBySerializable();
        System.out.println(p1 == p2);
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());

        p2.list.add("dddd");
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());
    }

    public static void reflectCloneTest(PrototypeMember p1){
        System.out.println("反射clone - 浅克隆！");

        PrototypeMember p2 = p1.cloneByReflect();
        System.out.println(p1 == p2);
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());

        p2.list.add("dddd");
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());
    }

    public static void jsonCloneTest(PrototypeMember p1){
        System.out.println("fastjson clone - 深克隆！");

        PrototypeMember p2 = p1.deepCloneByJSON();
        System.out.println(p1 == p2);
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());

        p2.list.add("dddd");
        System.out.println("p1 = " + p1.toString());
        System.out.println("p2 = " + p2.toString());
    }
}
