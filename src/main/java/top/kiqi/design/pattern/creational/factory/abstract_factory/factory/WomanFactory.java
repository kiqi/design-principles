package top.kiqi.design.pattern.creational.factory.abstract_factory.factory;

import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.AbstractWoman;
import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.BlackWoman;
import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.WhiteWoman;
import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.YellowWoman;

public class WomanFactory implements AbstractHumanFactory{
    @Override
    public AbstractWoman createWhiteHuman() {
        return new WhiteWoman();
    }

    @Override
    public AbstractWoman createYellowHuman() {
        return new YellowWoman();
    }

    @Override
    public AbstractWoman createBlackHuman() {
        return new BlackWoman();
    }

    public AbstractWoman createWoman() {
        return new WhiteWoman();
    }
}
