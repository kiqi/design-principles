package top.kiqi.design.pattern.creational.factory.abstract_factory.produce;

public class YellowMan extends AbstractMan{
    @Override
    public void talk() {
        System.out.println("I am a yellow human!");
    }

    @Override
    public void showColor() {
        System.out.println("yellow!");
    }
}
