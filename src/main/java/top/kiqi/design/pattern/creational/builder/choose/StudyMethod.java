package top.kiqi.design.pattern.creational.builder.choose;

// 零件装配，产品对象
public class StudyMethod {
    private String methodTitle = "study method";
    private boolean readBook = true;
    private boolean watchVideos = false;
    private boolean doHomework = false;
    private boolean takeNotes = false;

    public String getMethodTitle() {
        return methodTitle;
    }

    public void setMethodTitle(String methodTitle) {
        this.methodTitle = methodTitle;
    }

    public boolean isWatchVideos() {
        return watchVideos;
    }

    public void setWatchVideos(boolean watchVideos) {
        this.watchVideos = watchVideos;
    }

    public boolean isDoHomework() {
        return doHomework;
    }

    public void setDoHomework(boolean doHomework) {
        this.doHomework = doHomework;
    }

    public boolean isTakeNotes() {
        return takeNotes;
    }

    public void setTakeNotes(boolean takeNotes) {
        this.takeNotes = takeNotes;
    }

    public void show(){
        System.out.print(methodTitle + " : ");
        System.out.println("readBook" +
                (watchVideos ? " - watchVideos":"") +
                (doHomework ? " - doHomework":"") +
                (takeNotes ? " - takeNotes":""));
    }
}
