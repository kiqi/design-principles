package top.kiqi.design.pattern.creational.builder.add;

import top.kiqi.design.pattern.creational.builder.choose.StudyMethod;

import java.util.ArrayList;
import java.util.List;

public class StudyPlan {
    private List<StudyTimeSlot> timeSlots = new ArrayList<>();
    private StudyMethod studyMethod = null;

    StudyPlan(StudyMethod studyMethod) {
        this.studyMethod = studyMethod;
    }

    void addSlot(int i, String title){
        timeSlots.add(new StudyTimeSlot(i,title));
    }

    public void show() {
        studyMethod.show();
        for(StudyTimeSlot s : timeSlots){
            System.out.println(s.time + " hours - " + s.title);
        }
    }

    class StudyTimeSlot{
        int time;
        String title;

        StudyTimeSlot(int time, String title) {
            this.time = time;
            this.title = title;
        }
    }
}
