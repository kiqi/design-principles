package top.kiqi.design.pattern.creational.prototype;

public class Parent {
    // 反序列化时，会在继承链中，找到离实际对象最近的一个没有实现Serializable接口的父类，并调用其构造方法
    public Parent(){
        System.out.println("parent！");
    }
}
