package top.kiqi.design.pattern.creational.factory.factory_pool;

import java.sql.Connection;

public class Test {
    public static void main(String[] args) {
        ConnManager connManager = ConnManager.getInstance();
        Connection conn1 = connManager.getConnection();

        Connection conn2 = connManager.getConnection();

        System.out.println(conn1);
        System.out.println(conn2);
        connManager.show();

        connManager.freeConn(conn2);
        connManager.show();
    }
}
