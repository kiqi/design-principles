package top.kiqi.design.pattern.creational.singleton.hungry;

// 饿汉式单例，类文件初始化过程中进行单例实例化
public class HungrySingleton {
    private static final HungrySingleton instance = new HungrySingleton();

    private HungrySingleton(){
        if(instance != null){
            throw new RuntimeException("非法访问!");
        }
    }

    public static HungrySingleton getInstance(){
        return instance;
    }
}
