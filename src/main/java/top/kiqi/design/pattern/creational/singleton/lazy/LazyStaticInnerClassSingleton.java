package top.kiqi.design.pattern.creational.singleton.lazy;

/*
 * 内部静态类实现懒汉式单例
 *
 * 由于java类的加载机制，在加载LaztStaticInnerClassSingleton时，并不会加载LazyHolder类
 * 只有当调用getInstance方法时，才会对LazyHolder进行加载，从而实现懒加载
 */
public class LazyStaticInnerClassSingleton {

    private LazyStaticInnerClassSingleton(){
        if(LazyHolder.instance != null){
            throw new RuntimeException("非法访问!");
        }
    }

    public static LazyStaticInnerClassSingleton getInstance(){
        return LazyHolder.instance;
    }

    private static class LazyHolder{
        private static final LazyStaticInnerClassSingleton instance = new LazyStaticInnerClassSingleton();
    }
}
