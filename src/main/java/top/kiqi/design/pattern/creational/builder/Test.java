package top.kiqi.design.pattern.creational.builder;

import top.kiqi.design.pattern.creational.builder.add.StudyPlan;
import top.kiqi.design.pattern.creational.builder.add.StudyPlanBuilder;
import top.kiqi.design.pattern.creational.builder.choose.StudyMethod;
import top.kiqi.design.pattern.creational.builder.choose.StudyMethodBuilder;

// 建造者模式
public class Test {
    public static void main(String[] args) {
        // 产品指导类，提供创建预制的常规型号产品
        Director director = new Director();
        StudyPlan allDayStudyPlan = director.buildAllDaysStudyPlan("java");
        allDayStudyPlan.show();

        // 零件装配
        StudyMethodBuilder studyMethodBuilder = new StudyMethodBuilder();
        StudyMethod studyMethod = studyMethodBuilder.setStudyMethodTitle("Java study")
                                                    .addTakeNotes()
                                                    .addDoHomework()
                                                    .builder();
        studyMethod.show();

        // 顺序加工
        StudyPlanBuilder studyPlanBuilder = new StudyPlanBuilder(studyMethod);
        studyPlanBuilder.addReadBook(3);
        studyPlanBuilder.addTakeNotes(1);
        studyPlanBuilder.addWatchVideos(2);
        studyPlanBuilder.addDoHomework(1);
        studyPlanBuilder.addTakeNotes(2);
        StudyPlan studyPlan = studyPlanBuilder.builder();
        studyPlan.show();
    }
}
