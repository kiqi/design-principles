package top.kiqi.design.pattern.creational.factory.abstract_factory.produce;

public abstract class AbstractMan implements IHuman{
    public void sex(){
        System.out.println("I am a man!");
    }
}
