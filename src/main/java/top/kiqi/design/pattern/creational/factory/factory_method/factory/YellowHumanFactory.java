package top.kiqi.design.pattern.creational.factory.factory_method.factory;

import top.kiqi.design.pattern.creational.factory.factory_method.produce.IHuman;
import top.kiqi.design.pattern.creational.factory.factory_method.produce.YellowHuman;

public class YellowHumanFactory implements AbstractHumanFactory{
    @Override
    public IHuman createHuman() {
        return new YellowHuman();
    }
}
