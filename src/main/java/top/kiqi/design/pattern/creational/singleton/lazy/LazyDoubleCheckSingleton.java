package top.kiqi.design.pattern.creational.singleton.lazy;

// 双重检查锁，懒汉式单例
public class LazyDoubleCheckSingleton {

    /* volatile 防止指令重排序
     *
     * NEW top/kiqi/design/pattern/singleton/lazy/Test
     * DUP
     * INVOKESPECIAL top/kiqi/design/pattern/singleton/lazy/Test.<init> ()V
     * PUTSTATIC top/kiqi/design/pattern/singleton/lazy/Test.t : Ltop/kiqi/design/pattern/singleton/lazy/Test;
     */
    private volatile static LazyDoubleCheckSingleton instance;

    private LazyDoubleCheckSingleton(){
        if(instance != null){
            throw new RuntimeException("非法访问!");
        }
    }

    public static LazyDoubleCheckSingleton getInstance(){
        if(instance == null){
            synchronized (LazyDoubleCheckSingleton.class){
                if(instance == null){
                    instance = new LazyDoubleCheckSingleton();
                }
            }
        }
        return instance;
    }
}
