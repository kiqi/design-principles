package top.kiqi.design.pattern.creational.factory.simple_factoy;

import top.kiqi.design.pattern.creational.factory.simple_factoy.factory.HumanFactory;
import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.BlackHuman;
import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.IHuman;
import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.WhiteHuman;
import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.YellowHuman;

public class Test {
    // 简单工厂模式 - 工厂类无父类接口，不易拓展。 拓展只能修改工厂类内部实现逻辑。
    public static void main(String[] args) {
        IHuman white = HumanFactory.createHuman(WhiteHuman.class);
        white.showColor();
        white.talk();

        IHuman yellow = HumanFactory.createHuman(YellowHuman.class);
        yellow.showColor();
        yellow.talk();

        IHuman black = HumanFactory.createHuman(BlackHuman.class);
        black.showColor();
        black.talk();
    }
}
