package top.kiqi.design.pattern.creational.factory.abstract_factory.produce;

public class WhiteWoman extends AbstractWoman{
    @Override
    public void talk() {
        System.out.println("I am a white human!");
    }

    @Override
    public void showColor() {
        System.out.println("white!");
    }
}
