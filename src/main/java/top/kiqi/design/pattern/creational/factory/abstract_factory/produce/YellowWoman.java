package top.kiqi.design.pattern.creational.factory.abstract_factory.produce;

public class YellowWoman extends AbstractWoman{
    @Override
    public void talk() {
        System.out.println("I am a yellow human!");
    }

    @Override
    public void showColor() {
        System.out.println("yellow!");
    }
}
