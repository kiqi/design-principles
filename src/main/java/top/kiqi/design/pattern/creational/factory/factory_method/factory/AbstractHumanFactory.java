package top.kiqi.design.pattern.creational.factory.factory_method.factory;

import top.kiqi.design.pattern.creational.factory.factory_method.produce.IHuman;

public interface AbstractHumanFactory {
    IHuman createHuman();
}
