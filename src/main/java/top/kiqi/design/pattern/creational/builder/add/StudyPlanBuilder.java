package top.kiqi.design.pattern.creational.builder.add;

import top.kiqi.design.pattern.creational.builder.choose.StudyMethod;

public class StudyPlanBuilder {

    private StudyPlan studyPlan = null;
    private StudyMethod studyMethod = null;

    public StudyPlanBuilder(StudyMethod studyMethod){
        this.studyMethod = studyMethod;
        this.studyPlan = new StudyPlan(studyMethod);
    }

    public void addReadBook(int i){
        studyPlan.addSlot(i,"readBook");
    }

    public void addDoHomework(int i){
        if(studyMethod.isDoHomework()){
            studyPlan.addSlot(i, "doHomework");
        } else {
            System.out.println("    DoHomework - Measures table do not has such measure!");
        }
    }

    public void addWatchVideos(int i){
        if(studyMethod.isWatchVideos()) {
            studyPlan.addSlot(i, "watchVideos");
        } else {
            System.out.println("    WatchVideos - Measures table do not has such measure!");
        }
    }

    public void addTakeNotes(int i){
        if(studyMethod.isTakeNotes()) {
            studyPlan.addSlot(i, "takeNotes");
        } else {
            System.out.println("    TakeNotes - Measures table do not has such measure!");
        }
    }

    public StudyPlan builder(){
        return studyPlan;
    }
}
