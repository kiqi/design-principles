package top.kiqi.design.pattern.creational.factory.abstract_factory.factory;

import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.IHuman;

public interface AbstractHumanFactory {
    IHuman createWhiteHuman();
    IHuman createYellowHuman();
    IHuman createBlackHuman();
}
