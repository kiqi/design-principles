package top.kiqi.design.pattern.creational.factory.factory_method.produce;

public class YellowHuman implements IHuman{
    @Override
    public void talk() {
        System.out.println("I am a yellow human!");
    }

    @Override
    public void showColor() {
        System.out.println("yellow!");
    }
}
