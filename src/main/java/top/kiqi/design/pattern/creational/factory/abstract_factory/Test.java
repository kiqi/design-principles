package top.kiqi.design.pattern.creational.factory.abstract_factory;

import top.kiqi.design.pattern.creational.factory.abstract_factory.factory.AbstractHumanFactory;
import top.kiqi.design.pattern.creational.factory.abstract_factory.factory.ManFactory;
import top.kiqi.design.pattern.creational.factory.abstract_factory.factory.WomanFactory;
import top.kiqi.design.pattern.creational.factory.abstract_factory.produce.IHuman;

public class Test {
    // 抽象工厂方法，用于生成产品簇 --- 产品具有两个维度的区分方法，（根据需求，每一个具体的工厂实例负责某一种产品不同品牌的生产/负责某一品牌不同产品的生产）。
    public static void main(String[] args) {
        AbstractHumanFactory manFactory = new ManFactory();
        IHuman whiteMan = manFactory.createWhiteHuman();
        whiteMan.showColor();
        whiteMan.talk();
        whiteMan.sex();

        IHuman yellowMan = manFactory.createYellowHuman();
        yellowMan.showColor();
        yellowMan.talk();
        yellowMan.sex();

        IHuman blackMan = manFactory.createBlackHuman();
        blackMan.showColor();
        blackMan.talk();
        blackMan.sex();

        AbstractHumanFactory womanFactory = new WomanFactory();
        IHuman whiteWoman = womanFactory.createWhiteHuman();
        whiteWoman.showColor();
        whiteWoman.talk();
        whiteWoman.sex();

        IHuman yellowWoman = womanFactory.createYellowHuman();
        yellowWoman.showColor();
        yellowWoman.talk();
        yellowWoman.sex();

        IHuman blackWoman = womanFactory.createBlackHuman();
        blackWoman.showColor();
        blackWoman.talk();
        blackWoman.sex();
    }
}
