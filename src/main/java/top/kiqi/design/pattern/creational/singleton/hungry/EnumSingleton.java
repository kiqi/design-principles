package top.kiqi.design.pattern.creational.singleton.hungry;

// 枚举式单例(饿汉) - 可避免反射破坏等
public enum EnumSingleton {
    instance;

    // 其实这段方法并没有太大意义，不过习惯使用getInstance表示单例，直接用EnumSingleton.instance也可。
    public static EnumSingleton getInstance(){return instance;}

    // 以下为应用属性和代码
    private int index = 0;

    public synchronized void add(){
        index++;
    }

    public synchronized void print(){
        System.out.println(index);
    }
}
