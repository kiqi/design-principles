package top.kiqi.design.pattern.creational.singleton.lazy;

// 简单的懒汉式单例，可能引起线程不安全的情况
public class LazySimpleSingleton {
    private static LazySimpleSingleton instance;

    private LazySimpleSingleton() {
        if(instance != null){
            throw new RuntimeException("非法访问!");
        }
    }

    public static LazySimpleSingleton getInstance() {
        if(instance == null){
            instance = new LazySimpleSingleton();
        }
        return instance;
    }
}
