package top.kiqi.design.pattern.creational.factory.factory_method;

import top.kiqi.design.pattern.creational.factory.factory_method.factory.BlackHumanFactory;
import top.kiqi.design.pattern.creational.factory.factory_method.factory.WhiteHumanFactory;
import top.kiqi.design.pattern.creational.factory.factory_method.factory.YellowHumanFactory;
import top.kiqi.design.pattern.creational.factory.factory_method.produce.IHuman;

public class Test {
    // 工厂方法模式，工厂类有统一的父类接口，调用方只关心产品对应的工厂类即可。
    public static void main(String[] args) {
        IHuman white = new WhiteHumanFactory().createHuman();
        white.showColor();
        white.talk();

        IHuman yellow = new YellowHumanFactory().createHuman();
        yellow.showColor();
        yellow.talk();

        IHuman black = new BlackHumanFactory().createHuman();
        black.showColor();
        black.talk();
    }
}
