package top.kiqi.design.pattern.creational.factory.factory_method.factory;

import top.kiqi.design.pattern.creational.factory.factory_method.produce.BlackHuman;
import top.kiqi.design.pattern.creational.factory.factory_method.produce.IHuman;

public class BlackHumanFactory implements AbstractHumanFactory{
    @Override
    public IHuman createHuman() {
        return new BlackHuman();
    }
}
