package top.kiqi.design.pattern.creational.factory.simple_factoy.factory;

import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.BlackHuman;
import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.IHuman;
import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.WhiteHuman;
import top.kiqi.design.pattern.creational.factory.simple_factoy.produce.YellowHuman;

public class HumanFactory {
    public static <T extends IHuman> T createHuman(Class<T> t){
        IHuman human = null;
        try {
             human = (IHuman) Class.forName(t.getName()).newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        return (T)human;
    }

    public static <T extends IHuman> T createHuman(String color){
        IHuman human = null;
        if("white".equals(color)){
            human = new WhiteHuman();
        }else if("yellow".equals(color)){
            human = new YellowHuman();
        }else if("black".equals(color)){
            human = new BlackHuman();
        }
        return (T)human;
    }
}
