package top.kiqi.design.pattern.creational.prototype;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

// 原型模式，原型类作为抽象类,主要负责原型的相关数据和方法
public class PrototypeMember extends Parent implements Cloneable, Serializable {
    protected String username;
    protected List<String> list;

    protected PrototypeMember() {
    }

    public PrototypeMember(String username, List<String> list) {
        this.username = username;
        this.list = list;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    // java自带cloneable接口浅克隆 - 通过内存数据复制实现
    @Override
    public PrototypeMember clone(){
        try {
            return (PrototypeMember)super.clone();
        } catch (CloneNotSupportedException e){
            e.printStackTrace();
            return null;
        }
    }

    // 使用反射完成clone浅克隆
    public PrototypeMember cloneByReflect(){
        try {
            Class clazz = this.getClass();
            PrototypeMember result = (PrototypeMember)clazz.newInstance();
            for(Field filed : clazz.getDeclaredFields()){
                filed.setAccessible(true);
                filed.set(result,filed.get(this));
            }
            return result;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    // java自带cloneable，通过递归将所有引用属性都进行clone后实现深克隆
    public PrototypeMember deepcloneByCloneable(){
        try {
            PrototypeMember prototypeMember = (PrototypeMember)super.clone();
            prototypeMember.list = (List)((ArrayList)list).clone();
            return prototypeMember;
        } catch (CloneNotSupportedException e){
            e.printStackTrace();
            return null;
        }
    }

    // java自带Serializable，通过obj序列化成字节数组，再反序列实现clone
    public PrototypeMember deepCloneBySerializable(){
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(this);

            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (PrototypeMember) ois.readObject();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    // 使用json实现深克隆
    public PrototypeMember deepCloneByJSON(){
        String json = JSONObject.toJSONString(this);
        System.out.println(json);
        return JSON.parseObject(json,PrototypeMember.class);
    }

    @Override
    public String toString() {
        return "PrototypeMember{" +
                "username='" + username + '\'' +
                ", list=" + list +
                '}';
    }
}
