package top.kiqi.design.pattern.creational.factory.simple_factoy.produce;

public class WhiteHuman implements IHuman{
    @Override
    public void talk() {
        System.out.println("I am a white human!");
    }

    @Override
    public void showColor() {
        System.out.println("white!");
    }
}
