package top.kiqi.design.pattern.creational.factory.simple_factoy.produce;

public class BlackHuman implements IHuman{
    @Override
    public void talk() {
        System.out.println("I am a black human!");
    }

    @Override
    public void showColor() {
        System.out.println("black!");
    }
}
