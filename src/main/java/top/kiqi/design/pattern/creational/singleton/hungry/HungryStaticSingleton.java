package top.kiqi.design.pattern.creational.singleton.hungry;

import java.io.Serializable;

// 饿汉式单例，类文件初始化过程中进行单例实例化
public class HungryStaticSingleton implements Serializable {
    private static final HungryStaticSingleton instance;

    static {
        instance = new HungryStaticSingleton();
    }

    private HungryStaticSingleton(){
        if(instance != null){
            throw new RuntimeException("非法访问!");
        }
    };

    public static HungryStaticSingleton getInstance(){
        return instance;
    }

    // 防止反序列话破坏单例。
    public Object readResolve(){
        return instance;
    };
}
