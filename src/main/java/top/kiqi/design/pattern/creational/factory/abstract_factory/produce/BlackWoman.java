package top.kiqi.design.pattern.creational.factory.abstract_factory.produce;

public class BlackWoman extends AbstractWoman{
    @Override
    public void talk() {
        System.out.println("I am a black human!");
    }

    @Override
    public void showColor() {
        System.out.println("black!");
    }
}
