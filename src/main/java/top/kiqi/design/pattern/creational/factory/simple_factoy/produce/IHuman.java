package top.kiqi.design.pattern.creational.factory.simple_factoy.produce;

public interface IHuman {
    void talk();
    void showColor();
}
