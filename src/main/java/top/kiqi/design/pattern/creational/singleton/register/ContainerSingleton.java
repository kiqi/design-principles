package top.kiqi.design.pattern.creational.singleton.register;

import java.util.concurrent.ConcurrentHashMap;

// 注册式单例，自定义容器
public class ContainerSingleton{
    private static ConcurrentHashMap<String,Object> ioc = new ConcurrentHashMap<>();

    private ContainerSingleton(){
        throw new RuntimeException("非法访问!");
    }

    // 方法一：双重校验锁
//    public static Object getInstance(String className){
//        if(!ioc.contains(className)){
//            synchronized (ContainerSingleton.class){
//                if(!ioc.contains(className)){
//                    try{
//                        Object instance = Class.forName(className).newInstance();
//                        ioc.put(className,instance);
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
//        return ioc.get(className);
//    }

    // 方法二：ConcurrentHashMap$putIfAbsent()方法
    public static Object getInstance(String className){
        if(!ioc.contains(className)){
            try {
                ioc.putIfAbsent(className, Class.forName(className).newInstance());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return ioc.get(className);
    }
}
