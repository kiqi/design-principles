package top.kiqi.design.pattern.creational.factory.abstract_factory.produce;

public interface IHuman {
    void talk();
    void showColor();
    void sex();
}
