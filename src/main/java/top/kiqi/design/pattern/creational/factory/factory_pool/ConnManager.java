package top.kiqi.design.pattern.creational.factory.factory_pool;

import java.sql.Connection;

/**
 * 数据库连接管理类
 * @author tanyongde
 */
public class ConnManager {

    private static Pool dbPool;

    private static ConnManager instance = null;// 单例对象


    /**
     * 私有构造方法，禁止外部创建本类对象，想要获得本类对象，通过<code>getIstance</code>方法 使用了设计模式中的单例模式
     */
    private ConnManager() {}

    /**
     * 释放数据库连接对象
     */
    public void freeConn(Connection conn) {
        dbPool.freeConnection(conn);
    }

    /**
     * 返回当前连接管理类的一个对象
     */
    public synchronized static ConnManager getInstance() {
        if (null == instance) {
            instance = new ConnManager();
        }
        return instance;
    }

    /**
     * 从自定义数据库连接池中获得一个连接对象
     * @return
     */
    public synchronized Connection getConnection() {
        Connection conn = null;
        try
        {
            if(dbPool == null)
            {
                dbPool = DBConnectionPool.getInstance();
            }
            conn = dbPool.getConnection();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return conn;
    }

    public void show(){
        System.out.println(dbPool.getNumForActive());
        System.out.println(dbPool.getNumForFree());
    }
}
