package top.kiqi.design.pattern.creational.singleton;

import top.kiqi.design.pattern.creational.singleton.lazy.LazySimpleSingleton;
import top.kiqi.design.pattern.creational.singleton.lazy.LazyStaticInnerClassSingleton;
import top.kiqi.design.pattern.creational.singleton.threadlocal.ThreadLocalSingleton;
import top.kiqi.design.pattern.creational.singleton.hungry.EnumSingleton;
import top.kiqi.design.pattern.creational.singleton.hungry.HungryStaticSingleton;
import top.kiqi.design.pattern.creational.singleton.register.ContainerSingleton;

import java.io.*;
import java.lang.reflect.Constructor;

public class Test {

    public static void main(String[] args) {
        serializeTest();
    }

    // 容器式单例测试
    public static void containerTest(){
        Runnable runnable = () -> {
            System.out.println(Thread.currentThread().getName() + " - " + ContainerSingleton.getInstance("Test"));
        };

        new Thread(runnable).start();
        new Thread(runnable).start();
    }

    // 线程级单例
    public static void threadLocalTest(){
        new Thread(() -> {
            System.out.println(ThreadLocalSingleton.getInstance());
        }).start();

        System.out.println(ThreadLocalSingleton.getInstance());
    }

    // 防止反序列化破坏单例 - readResolve()
    public static void serializeTest(){
        HungryStaticSingleton s1 = HungryStaticSingleton.getInstance();
        HungryStaticSingleton s2 = null;

        // 序列化对象
        try(
                FileOutputStream fos = new FileOutputStream("HungryStaticSingleton.obj");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ){
            oos.writeObject(s1);
        }catch (Exception e){
            e.printStackTrace();
        }

        // 反序列化对象
        try(
                FileInputStream fis = new FileInputStream("HungryStaticSingleton.obj");
                ObjectInputStream ois = new ObjectInputStream(fis);
        ){
            s2 = (HungryStaticSingleton)ois.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }

        // HungryStaticSingleton设置了readResolve()方法，因此序列化获得了相同的对象
        System.out.println(s1);
        System.out.println(s2);
    }

    // 枚举类单例测试
    public static void enumTest(){
        EnumSingleton enumSingleton = EnumSingleton.instance;
        enumSingleton.add();
        enumSingleton.print();
        enumSingleton.add();
        enumSingleton.print();

        System.out.println(enumSingleton.name() + " - " + enumSingleton.ordinal());

        // 通过Enum类valueOf方法获取单例
        EnumSingleton a = Enum.valueOf(EnumSingleton.class,"instance");
        System.out.println(a.name() + " - " + a.ordinal());

        // 枚举类会维护一个EnumSingleton[] $VALUE，其中保存了枚举类中所有的EnumSingleton实例
        for(EnumSingleton e : EnumSingleton.values()){
            System.out.println(e.name() + " - " + e.ordinal());
        }
    }

    // 反射破坏单例。(反射可以暴力调用私有的构造方法，因此private类型构造方法的单例均可被反射破坏)
    public static void refTest(){
        try {
            Class<?> clazz = LazyStaticInnerClassSingleton.class;
            Constructor c = clazz.getDeclaredConstructor();
            c.setAccessible(true);  // 私有方法需要开启权限
            LazyStaticInnerClassSingleton instance = (LazyStaticInnerClassSingleton)c.newInstance();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    // 简单懒汉式单例 - 并行调试可能出现的线程不安全情况。
    public static void test(){
        Runnable runnable = () -> {
            LazySimpleSingleton instance = LazySimpleSingleton.getInstance();
            System.out.println(Thread.currentThread().getName() + " - " + instance);
        };

        Thread t1 = new Thread(runnable);
        Thread t2 = new Thread(runnable);

        t1.start();
        t2.start();
        System.out.println("End!");
    }
}
