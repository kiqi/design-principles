package top.kiqi.design.pattern.creational.builder;

import top.kiqi.design.pattern.creational.builder.add.StudyPlan;
import top.kiqi.design.pattern.creational.builder.add.StudyPlanBuilder;
import top.kiqi.design.pattern.creational.builder.choose.StudyMethod;
import top.kiqi.design.pattern.creational.builder.choose.StudyMethodBuilder;

// 产品生成指导类，提供常规版本产品的构建
public class Director {
    public StudyPlan buildAllDaysStudyPlan(String name){
        StudyMethodBuilder studyMethodBuilder = new StudyMethodBuilder();
        StudyMethod studyMethod = studyMethodBuilder.setStudyMethodTitle(name + " study")
                .addWatchVedeos()
                .addTakeNotes()
                .addDoHomework()
                .builder();

        StudyPlanBuilder studyPlanBuilder = new StudyPlanBuilder(studyMethod);
        studyPlanBuilder.addReadBook(3);
        studyPlanBuilder.addTakeNotes(1);
        studyPlanBuilder.addWatchVideos(2);
        studyPlanBuilder.addDoHomework(1);
        studyPlanBuilder.addTakeNotes(2);
        return studyPlanBuilder.builder();
    }
}
