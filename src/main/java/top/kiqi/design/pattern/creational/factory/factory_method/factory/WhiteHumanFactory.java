package top.kiqi.design.pattern.creational.factory.factory_method.factory;

import top.kiqi.design.pattern.creational.factory.factory_method.produce.IHuman;
import top.kiqi.design.pattern.creational.factory.factory_method.produce.WhiteHuman;

public class WhiteHumanFactory implements AbstractHumanFactory{
    @Override
    public IHuman createHuman() {
        return new WhiteHuman();
    }
}
