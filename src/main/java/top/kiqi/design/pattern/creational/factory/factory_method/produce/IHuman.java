package top.kiqi.design.pattern.creational.factory.factory_method.produce;

public interface IHuman {
    void talk();
    void showColor();
}
