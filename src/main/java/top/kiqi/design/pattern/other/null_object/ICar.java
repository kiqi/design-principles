package top.kiqi.design.pattern.other.null_object;

public interface ICar {
    boolean isNull();

    void run();
}
