package top.kiqi.design.pattern.other.null_object;

public class CarFactory {
    public ICar createCar(int money){
        return (money > 10 * 1024)? new CarA() : new NullCar();
    }
}
