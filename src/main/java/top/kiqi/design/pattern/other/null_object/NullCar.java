package top.kiqi.design.pattern.other.null_object;

public class NullCar implements ICar{
    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public void run() {
        System.out.println("Not have a car, can't run!");
    }
}
