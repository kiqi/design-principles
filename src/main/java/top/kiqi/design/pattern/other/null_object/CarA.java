package top.kiqi.design.pattern.other.null_object;

public class CarA implements ICar{
    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public void run() {
        System.out.println("The car is runing");
    }
}
