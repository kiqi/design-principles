package top.kiqi.design.pattern.other.null_object;

// 空对象模式 - 对于同一个接口，具有两种实现(具体对象类 和 空对象类)
// 当返回值为null时，返回空对象类，空对象类中的方法不做实际业务处理，而是定制输出相关报错信息(避免空指针异常)
public class Test {
    public static void main(String[] args) {
        CarFactory carFactory = new CarFactory();
        ICar car = carFactory.createCar((int)(Math.random() * 100 * 1024));     // return (money > 10 * 1024)? new CarA() : new NullCar();
        car.run();
    }
}
